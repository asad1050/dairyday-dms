import Controls from './layout/Controls'
import Login from './ui/Login'
import React from 'react'
import { render } from 'react-dom'

FlowRouter.route('/', {
	name: 'root',
	action: (params, qParams) => {
		if (Meteor.userId() == null){
			FlowRouter.go('/login');
		}else{

			render(<Controls comp="Dashboard"  />, document.getElementById('render-target'))
		}
	}
})

FlowRouter.route('/login', {
	name: 'login',
	action: (params, qParams) => {
		if (Meteor.userId() == null){
			render(<Login />, document.getElementById('render-target'))
		}else{
			FlowRouter.go('/');
		}

	}
})

FlowRouter.route('/items', {
	name: 'items',
	action: (params, qParams) => {
		if (Meteor.userId() == null){
			FlowRouter.go('/login');
		}else{
			Meteor.call('checkAdmin',(err,res)=>{
				if (err){
					FlowRouter.go('/');
				}else{
				render(<Controls comp="Items"  />, document.getElementById('render-target'))
				}})

		}
	}
})

FlowRouter.route('/customers', {
	name: 'customers',
	action: (params, qParams) => {
		if (Meteor.userId() == null){
			FlowRouter.go('/login');
		}else{
			Meteor.call('checkAdmin',(err,res)=>{
				if (err){
					FlowRouter.go('/');
				}else{
				render(<Controls comp="Customers"  />, document.getElementById('render-target'))
				}})

		}
	}
})

FlowRouter.route('/deliveryboys', {
	name: 'deliveryboys',
	action: (params, qParams) => {
		if (Meteor.userId() == null){
			FlowRouter.go('/login');
		}else{
			Meteor.call('checkAdmin',(err,res)=>{
				if (err){
					FlowRouter.go('/');
				}else{
				render(<Controls comp="DeliveryBoys"  />, document.getElementById('render-target'))
				}})

		}
	}
})

FlowRouter.route('/stockin', {
	name: 'stockin',
	action: (params, qParams) => {
		if (Meteor.userId() == null){
			FlowRouter.go('/login');
		}else{
			Meteor.call('checkAdminShopkeeper',(err,res)=>{
				if (err){
					FlowRouter.go('/');
				}else{
				render(<Controls comp="StockIn"  />, document.getElementById('render-target'))
				}})

		}
	}
})

FlowRouter.route('/delivery', {
	name: 'delivery',
	action: (params, qParams) => {
		if (Meteor.userId() == null){
			FlowRouter.go('/login');
		}else{
			Meteor.call('checkDeliveryBoy',(err,res)=>{
				if (err){
					FlowRouter.go('/');
				}else{
				render(<Controls comp="Delivery"  />, document.getElementById('render-target'))
				}})

		}
	}
})

FlowRouter.route('/billing', {
	name: 'billing',
	action: (params, qParams) => {
		if (Meteor.userId() == null){
			FlowRouter.go('/login');
		}else{
			Meteor.call('checkCustomer',(err,res)=>{
				if (err){
					FlowRouter.go('/');
				}else{
				render(<Controls comp="Billing"  />, document.getElementById('render-target'))
				}})

		}
	}
})

FlowRouter.route('/customersbalancereport', {
	name: 'customersbalancereport',
	action: (params, qParams) => {
		if (Meteor.userId() == null){
			FlowRouter.go('/login');
		}else{
			Meteor.call('checkAdmin',(err,res)=>{
				if (err){
					FlowRouter.go('/');
				}else{
				render(<Controls comp="CustomersBalanceReport"  />, document.getElementById('render-target'))
				}})

		}
	}
})

FlowRouter.route('/accountsreport', {
	name: 'accountsreport',
	action: (params, qParams) => {
		if (Meteor.userId() == null){
			FlowRouter.go('/login');
		}else{
			Meteor.call('checkAdmin',(err,res)=>{
				if (err){
					FlowRouter.go('/');
				}else{
				render(<Controls comp="AccountsReport"  />, document.getElementById('render-target'))
				}})

		}
	}
})

FlowRouter.route('/balancedeposit', {
	name: 'balancedeposit',
	action: (params, qParams) => {
		if (Meteor.userId() == null){
			FlowRouter.go('/login');
		}else{
			Meteor.call('checkDeliveryBoy',(err,res)=>{
				if (err){
					FlowRouter.go('/');
				}else{
				render(<Controls comp="BalanceDeposit"  />, document.getElementById('render-target'))
				}})

		}
	}
})

FlowRouter.route('/shopkeepers', {
	name: 'shopkeepers',
	action: (params, qParams) => {
		if (Meteor.userId() == null){
			FlowRouter.go('/login');
		}else{
			Meteor.call('checkAdmin',(err,res)=>{
				if (err){
					FlowRouter.go('/');
				}else{
				render(<Controls comp="Shopkeepers"  />, document.getElementById('render-target'))
				}})

		}
	}
})

FlowRouter.route('/expenses', {
	name: 'expenses',
	action: (params, qParams) => {
		if (Meteor.userId() == null){
			FlowRouter.go('/login');
		}else{
			Meteor.call('checkAdmin',(err,res)=>{
				if (err){
					FlowRouter.go('/');
				}else{
				render(<Controls comp="Expenses"  />, document.getElementById('render-target'))
				}})

		}
	}
})
FlowRouter.route('/deliveryboystock', {
	name: 'deliveryboystock',
	action: (params, qParams) => {
		if (Meteor.userId() == null){
			FlowRouter.go('/login');
		}else{
			Meteor.call('checkAdminShopkeeper',(err,res)=>{
				if (err){
					FlowRouter.go('/');
				}else{
				render(<Controls comp="DeliveryBoyStockIn"  />, document.getElementById('render-target'))
				}})

		}
	}
})

FlowRouter.route('/pointofsale', {
	name: 'pointofsale',
	action: (params, qParams) => {
		if (Meteor.userId() == null){
			FlowRouter.go('/login');
		}else{
			Meteor.call('checkShopkeeper',(err,res)=>{
				if (err){
					FlowRouter.go('/');
				}else{
				render(<Controls comp="PointOfSale"  />, document.getElementById('render-target'))
				}})

		}
	}
})

FlowRouter.route('/profitlossreport', {
	name: 'profitlossreport',
	action: (params, qParams) => {
		if (Meteor.userId() == null){
			FlowRouter.go('/login');
		}else{
			Meteor.call('checkAdmin',(err,res)=>{
				if (err){
					FlowRouter.go('/');
				}else{
				render(<Controls comp="ProfitLossReport"  />, document.getElementById('render-target'))
				}})

		}
	}
})

FlowRouter.route('/adminpaymentreport', {
	name: 'adminpaymentreport',
	action: (params, qParams) => {
		if (Meteor.userId() == null){
			FlowRouter.go('/login');
		}else{
			Meteor.call('checkAdmin',(err,res)=>{
				if (err){
					FlowRouter.go('/');
				}else{
				render(<Controls comp="AdminPaymentReport"  />, document.getElementById('render-target'))
				}})

		}
	}
})

FlowRouter.route('/customerpaymentreport', {
	name: 'customerpaymentreport',
	action: (params, qParams) => {
		if (Meteor.userId() == null){
			FlowRouter.go('/login');
		}else{
			Meteor.call('checkCustomer',(err,res)=>{
				if (err){
					FlowRouter.go('/');
				}else{
				render(<Controls comp="CustomerPaymentReport"  />, document.getElementById('render-target'))
				}})

		}
	}
})

FlowRouter.route('/changepassword', {
	name: 'changepassword',
	action: (params, qParams) => {
		if (Meteor.userId() == null){
			FlowRouter.go('/login');
		}else{
			render(<Controls comp="ChangePassword"  />, document.getElementById('render-target'))

		}
	}
})
