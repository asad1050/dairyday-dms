import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import ShopkeeperInsert from './ShopkeeperInsert'
import ShopkeepersList from './ShopkeepersList'
import UpdateShopkeeper from './UpdateShopkeeper'


export default class Shopkeepers extends Component {
	constructor(props){
		super(props)
	}

	render() {


		return (
		<div>
		<ShopkeeperInsert/>
		<ShopkeepersList/>
		<UpdateShopkeeper/>
		</div>

		)
	}

}
