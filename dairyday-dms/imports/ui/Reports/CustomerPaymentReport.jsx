import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import {Payments,Customers,DeliveryBoys} from '../../api/collections'
import $ from 'jquery';
import moment from 'moment';
import 'bootstrap-datepicker';

import TrackerReact from 'meteor/ultimatejs:tracker-react';


export default class CustomerPaymentReport extends TrackerReact(React.Component) {

	constructor(props){
		super(props)
		this.state = {
          subscription: {
            payments: Meteor.subscribe('payments'),
						customers: Meteor.subscribe('customers'),
						deliveryboys: Meteor.subscribe('deliveryboys')
          },
					payments:[],
					totalpayments:0
        }
	}

	componentWillUnmount() {
		this.state.subscription.payments.stop();
		this.state.subscription.customers.stop();
		this.state.subscription.deliveryboys.stop();
	}

	componentDidMount(){

		$('.datepicker').datepicker({
			format: 'mm-dd-yyyy',
        clearButton: true,
        weekStart: 1,
        time: false,
				autoclose: true
    });
	}

	customers(){
		return Customers.find().fetch();
	}

	deliveryboy(id){
		return DeliveryBoys.findOne({ '_id':id }).name;
	}
	dateformat(date){
		return moment(date).format('MMMM Do YYYY');
	}

	handleSubmit(event) {
    event.preventDefault();
		const datefrom = ReactDOM.findDOMNode(this.refs.datefrom).value.trim();
		const dateto = ReactDOM.findDOMNode(this.refs.dateto).value.trim();
		var customerid = Customers.findOne({ 'userid':Meteor.userId() })._id;
		var datef = new Date(datefrom);
		var datet = new Date(dateto);
		datef = datef.toISOString();
		datet = datet.toISOString();
		var payments = Payments.find({'date' : { $gte : datef, $lt: datet }, 'customerid':customerid}).fetch();


		var npayments = payments;

		var totalpayments = 0;
		for (var i=0;i<payments.length;i++){
			npayments[i].date = this.dateformat(payments[i].date);
			npayments[i].deliveryboyid = this.deliveryboy(payments[i].deliveryboyid);
			npayments[i].amount = payments[i].amount;
			totalpayments = parseFloat(totalpayments) + parseFloat(payments[i].amount);
		}

		this.setState({
			payments:npayments,
			totalpayments:totalpayments
		 });

  }

	render() {
		let payments = this.state.payments;
		let customers = this.customers();
		const divStyle = {
		  paddingBottom: '50px'
		};
		return (
			<div>

				<div className="box box-info" >
								<div className="box-header">
									<h3 className="box-title">Information</h3>

								</div>

								<div className="box-body" style={divStyle}>
									<p>Select two dates to get you deliveries between two dates.<br></br> Thanks!</p>
								</div>




							</div>




			<div className="box box-info">
	            <div className="box-header">
	              <h3 className="box-title">Deliveries</h3>

	            </div>

							<div className="box-body" >
								<div class="row">
										<form className="new-task" onSubmit={this.handleSubmit.bind(this)} >
            <div class="col-md-6">
								<div className="form-group">

																<label>Date From:</label>

																<div className="input-group date">
																	<div className="input-group-addon">
																		<i className="fa fa-calendar"></i>
																	</div>
																	<input type="text" ref="datefrom" className="form-control pull-right datepicker" />
																</div>

															</div>


														</div>

														<div class="col-md-6">
																<div className="form-group">

																								<label>Date To:</label>

																								<div className="input-group date">
																									<div className="input-group-addon">
																										<i className="fa fa-calendar"></i>
																									</div>
																									<input type="text" ref="dateto" className="form-control pull-right datepicker" />
																								</div>

																							</div>


																						</div>
																							<div class="col-md-12">

<div className="form-group">
																						<button type="submit" class="btn btn-info pull-right">Retrive</button>
</div>
</div>
																					</form>
													</div>
							</div>

							<div className="box-body table-responsive no-padding">
							 <table className="table table-hover">
								 <tbody>
									 <tr>

									 <th>DeliveryBoy</th>
									 <th>Date</th>
									 <th>Amount</th>
								 </tr>
								 {payments.map(function(payment, index){
								 return ( <tr key={ index }>
										 <td>{payment.deliveryboyid}</td>
									 <td>{payment.date}</td>
									  <td>{payment.amount}</td>
									 </tr>);
								 })}

									 <tr>
									<td>  <i className="fa fa-align-justify"></i>  </td>
								<td><i className="fa fa-align-justify"></i> </td>
									<td> <i className="fa fa-align-justify"></i> </td>
									 </tr>

									<tr>
								 <td>  Total Payment:  </td>
							 <td></td>
										<td>{this.state.totalpayments}</td>
									</tr>


							 </tbody></table>
						 </div>


	          </div>
						</div>
		)
	}

}
