import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import swal from 'sweetalert'


export default class Login extends Component {
	constructor(props){
		super(props)
	}



	componentDidMount(){
		document.body.classList.add('hold-transition');
		document.body.classList.add('login-page');
	}

	handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
    const username = ReactDOM.findDOMNode(this.refs.username).value.trim();
		const password = ReactDOM.findDOMNode(this.refs.password).value.trim();

    console.log(username);
		console.log(password);
		Meteor.loginWithPassword(username,password,(err,res)=>{
			if (err){
				console.log(err.message);
				swal("Error", err.message, "error");
			}else{
			swal("Success", "Successfully Logged in.", "success");
			window.location.href = '/'

			}
		})
    // Clear form
    ReactDOM.findDOMNode(this.refs.username).value = '';
		ReactDOM.findDOMNode(this.refs.password).value = '';
		ReactDOM.findDOMNode(this.refs.username).focus = '';
  }

	render() {
		const boxStyle = {
	  paddingTop: '5%'
		};

		return (
			<div className="login-box" style={boxStyle}>
			  <div className="login-logo">
			    <a href="../../index2.html"><b>Dairy</b>Day</a>
			  </div>

			  <div className="login-box-body">
			    <p className="login-box-msg">Sign in to start your session</p>

			    <form onSubmit={this.handleSubmit.bind(this)} >
			      <div className="form-group has-feedback">
			        <input type="text" ref="username" className="form-control" placeholder="Username"/>
			        <span className="glyphicon glyphicon-user form-control-feedback"></span>
			      </div>
			      <div className="form-group has-feedback">
			        <input type="password" ref="password" className="form-control" placeholder="Password"/>
			        <span className="glyphicon glyphicon-lock form-control-feedback"></span>
			      </div>
			      <div className="row">
			        <div className="col-xs-8">

			        </div>

			        <div className="col-xs-4">
			          <button type="submit" className="btn btn-primary btn-block btn-flat">Sign In</button>
			        </div>

			      </div>
			    </form>


			  </div>

			</div>

		)
	}

}
