import {Users, StockIn, Items, Customers, Deliveries, DeliveryBoys, Payments,DeliveryBoyStock,PointOfSales,Expenses,Shopkeepers} from '../imports/api/collections'

if (Meteor.isServer){
  Meteor.publish('stockin',  function stockinPublication() {
    if (Meteor.userId()){
    return StockIn.find();}
    else{
      throw new Meteor.Error('Not logged in')
    }
  });



  Meteor.publish('user',  function usersPublication() {
    if (Meteor.userId()){
    return Users.find(Meteor.userId());
  }
    else{
      throw new Meteor.Error('Not logged in')
    }
  });



  Meteor.publish('items',  function itemsPublication() {
    if (Meteor.userId()){
    return Items.find();}
    else{
      throw new Meteor.Error('Not logged in')
    }
  });

  Meteor.publish('customers',  function customersPublication() {
    if (Meteor.userId()){
    return Customers.find();}
    else{
      throw new Meteor.Error('Not logged in')
    }
  });

  Meteor.publish('deliveries',  function deliveriesPublication() {
    if (Meteor.userId()){
    return Deliveries.find();}
    else{
      throw new Meteor.Error('Not logged in')
    }
  });

  Meteor.publish('deliveryboys',  function deliveryboysPublication() {
    if (Meteor.userId()){
    return DeliveryBoys.find();}
    else{
      throw new Meteor.Error('Not logged in')
    }
  });

  Meteor.publish('payments',  function paymentsPublication() {
    if (Meteor.userId()){
    return Payments.find();}
    else{
      throw new Meteor.Error('Not logged in')
    }
  });

  Meteor.publish('deliveryboystock',  function deliveryBoyStockPublication() {
    if (Meteor.userId()){
    return DeliveryBoyStock.find();}
    else{
      throw new Meteor.Error('Not logged in')
    }
  });

  Meteor.publish('pointofsales',  function posPublication() {
    if (Meteor.userId()){
    return PointOfSales.find();}
    else{
      throw new Meteor.Error('Not logged in')
    }
  });

  Meteor.publish('expenses',  function expensesPublication() {
    if (Meteor.userId()){
    return Expenses.find();}
    else{
      throw new Meteor.Error('Not logged in')
    }
  });

  Meteor.publish('shopkeepers',  function expensesPublication() {
    if (Meteor.userId()){
    return Shopkeepers.find();}
    else{
      throw new Meteor.Error('Not logged in')
    }
  });


}
