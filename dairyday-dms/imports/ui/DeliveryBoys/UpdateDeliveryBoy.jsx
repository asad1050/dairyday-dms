import React, {Component} from 'react'
import ReactDOM from 'react-dom'


export default class UpdateDeliveryBoy extends Component {

	constructor(props){
		super(props)

	}

	handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
		const deliveryboyname = ReactDOM.findDOMNode(this.refs.upddeliveryboyname).value.trim();
		const deliveryboyaddress = ReactDOM.findDOMNode(this.refs.upddeliveryboyaddress).value.trim();
		const deliveryboycontactnumber = ReactDOM.findDOMNode(this.refs.upddeliveryboycontactnumber).value.trim();
		const deliveryboyid = ReactDOM.findDOMNode(this.refs.upddeliveryboyid).value.trim();
		const deliveryboypassword = ReactDOM.findDOMNode(this.refs.upddeliveryboypassword).value.trim();

		Meteor.call('updateDeliveryBoy',deliveryboyid, deliveryboyname,deliveryboyaddress, deliveryboycontactnumber, deliveryboypassword,(err,res)=>{
			if (err){
				swal("Error", err.message, "error");
			}else{
			swal("Success", "DeliveryBoy successfully Updated.", "success");
			document.getElementById("updmodalclose").click();
			ReactDOM.findDOMNode(this.refs.upddeliveryboyname).value = '';
			ReactDOM.findDOMNode(this.refs.upddeliveryboyaddress).value = '';
			ReactDOM.findDOMNode(this.refs.upddeliveryboycontactnumber).value = '';
			ReactDOM.findDOMNode(this.refs.upddeliveryboyid).value = '';
			ReactDOM.findDOMNode(this.refs.upddeliveryboypassword).value = '';
			}
		})

  }

	render() {

		return (
			<div>
				<div class="modal fade" id="modal-default">
				 <div class="modal-dialog">
					 <div class="modal-content">
						 <div class="modal-header">
							 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
								 <span aria-hidden="true">&times;</span></button>
							 <h4 class="modal-title">Update Delivery Boy</h4>
						 </div>
						 	<form  onSubmit={this.handleSubmit.bind(this)} >
						 <div class="modal-body">
							 <div className="form-group">
 									 <label>DeliveryBoy Name</label>
 									 <input type="text" id="upddeliveryboyname" ref="upddeliveryboyname" className="form-control" placeholder="Enter DeliveryBoy Name"/>
 								 </div>
								 <input type="hidden" id="upddeliveryboyid" ref="upddeliveryboyid" className="form-control" placeholder="Enter DeliveryBoy Name"/>

 						<div className="form-group">
 								 <label>DeliveryBoy Address</label>
 								 <input type="text" id="upddeliveryboyaddress" ref="upddeliveryboyaddress" className="form-control" placeholder="Enter DeliveryBoy Address"/>
 							 </div>
 							 <div className="form-group">
 										<label>DeliveryBoy Contact #</label>
 										<input type="text" id="upddeliveryboycontactnumber" ref="upddeliveryboycontactnumber" className="form-control" placeholder="Enter DeliveryBoy Contact #"/>
 									</div>

										 <div className="form-group">
													<label>DeliveryBoy Password</label>
													<input type="text" id="upddeliveryboypassword" ref="upddeliveryboypassword" className="form-control" placeholder="Enter Password if you want to change"/>
												</div>
						 </div>
						 <div class="modal-footer">
							 <button type="button" id="updmodalclose" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
							 <button type="submit" class="btn btn-primary" >Save changes</button>
						 </div>
					 </form>
					 </div>

				 </div>

			 </div>

			</div>

		)
	}

}
