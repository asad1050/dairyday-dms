import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import swal from 'sweetalert'
import {Items,DeliveryBoys} from '../../api/collections'
import TrackerReact from 'meteor/ultimatejs:tracker-react';


export default class StockInInsert extends TrackerReact(React.Component) {
	constructor(props){
		super(props)
		this.state = {
          subscription: {
            items: Meteor.subscribe('items'),
						deliveryboys: Meteor.subscribe('deliveryboys')
          }
        }
	}

	componentWillUnmount() {
		this.state.subscription.items.stop();
		this.state.subscription.deliveryboys.stop();
	}

	items(){
		return Items.find().fetch();
	}
	deliveryboys(){
		return DeliveryBoys.find().fetch();
	}

	handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
		const itemid = ReactDOM.findDOMNode(this.refs.itemid).value.trim();
		const deliveryboyid = ReactDOM.findDOMNode(this.refs.deliveryboyid).value.trim();
		const quantity = ReactDOM.findDOMNode(this.refs.quantity).value.trim();
		Meteor.call('insertDeliveryBoyStockIn',itemid,deliveryboyid,quantity,(err,res)=>{
			if (err){
				swal("Error", err.message, "error");
			}else{
			swal("Success", "DeliveryBoy stock added successfully.", "success");
			// Clear form

			ReactDOM.findDOMNode(this.refs.quantity).value = '';

			}
		})

  }

	render() {
		let items = this.items();
		let deliveryboys = this.deliveryboys();
		return (
			<div className="box box-success">
		        <div className="box-header with-border">
		          <h3 className="box-title">Insert Quantity</h3>

		        </div>
	<form className="new-task" onSubmit={this.handleSubmit.bind(this)} >
		        <div className="box-body">

							<div class="row">
  <div class="col-md-12">

		<div class="form-group">
				<label>Select Item</label>
				<select ref="itemid" class="form-control">
					{items.map(function(item, index){
					return (<option key={ index } value={item._id} >{item.name}</option>);
					})}

				</select>
			</div>

			<div class="form-group">
					<label>Select DeliveryBoy</label>
					<select ref="deliveryboyid" class="form-control">
						{deliveryboys.map(function(deliveryboy, index){
						return (<option key={ index } value={deliveryboy._id} >{deliveryboy.name} - {deliveryboy.contactnumber}</option>);
						})}

					</select>
				</div>



						<div className="form-group">
								 <label>Quantity</label>
								 <input type="text" ref="quantity" className="form-control" placeholder="Enter Quantity"/>
							 </div>
</div>
						</div>
					</div>



								<div class="box-footer">

		                 <button type="submit" class="btn btn-success pull-right">Insert</button>
		               </div>
									 </form>
		      </div>

		)
	}

}
