import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import {Items} from '../../api/collections'

export default class UpdateItemButton extends Component {

	constructor(props){
		super(props)
		this.state = {
          subscription: {
            items: Meteor.subscribe('items')
          }
        }
	}

	componentWillUnmount() {
		this.state.subscription.items.stop();
	}

	handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
    const id = ReactDOM.findDOMNode(this.refs.id).value.trim();
		var item = Items.findOne({'_id':id});
		document.getElementById("upditemname").value = item.name;
		document.getElementById("upditemprice").value = item.price;
		document.getElementById("upditemcost").value = item.cost;
		document.getElementById("upditemid").value = id;

  }

	render() {

		return (
			<div>
				<form  onSubmit={this.handleSubmit.bind(this)} >
					<input type="hidden" value={this.props.id} ref="id" />
					<button type="submit"  class="btn btn-default btn-block" data-toggle="modal" data-target="#modal-default">
												<i class="fa fa-edit"></i>
											 </button>
				</form>
			</div>

		)
	}

}
