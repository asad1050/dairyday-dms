import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import CustomerInsert from './CustomerInsert'
import CustomersList from './CustomersList'
import UpdateCustomer from './UpdateCustomer'


export default class Customers extends Component {
	constructor(props){
		super(props)
	}

	render() {


		return (
		<div>
		<CustomerInsert/>
		<CustomersList/>
		<UpdateCustomer/>
		</div>

		)
	}

}
