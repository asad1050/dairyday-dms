import React, {Component} from 'react'
import ReactDOM from 'react-dom'


export default class UpdateCustomer extends Component {

	constructor(props){
		super(props)

	}

	handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
		const customername = ReactDOM.findDOMNode(this.refs.updcustomername).value.trim();
		const customeraddress = ReactDOM.findDOMNode(this.refs.updcustomeraddress).value.trim();
		const customercontactnumber = ReactDOM.findDOMNode(this.refs.updcustomercontactnumber).value.trim();
		const customerid = ReactDOM.findDOMNode(this.refs.updcustomerid).value.trim();
		const customerpassword = ReactDOM.findDOMNode(this.refs.updcustomerpassword).value.trim();

		Meteor.call('updateCustomer',customerid, customername,customeraddress, customercontactnumber, customerpassword,(err,res)=>{
			if (err){
				swal("Error", err.message, "error");
			}else{
			swal("Success", "Customer successfully Updated.", "success");
			document.getElementById("updmodalclose").click();
			ReactDOM.findDOMNode(this.refs.updcustomername).value = '';
			ReactDOM.findDOMNode(this.refs.updcustomeraddress).value = '';
			ReactDOM.findDOMNode(this.refs.updcustomercontactnumber).value = '';
			ReactDOM.findDOMNode(this.refs.updcustomerid).value = '';
			ReactDOM.findDOMNode(this.refs.updcustomerpassword).value = '';
			}
		})

  }

	render() {

		return (
			<div>
				<div class="modal fade" id="modal-default">
				 <div class="modal-dialog">
					 <div class="modal-content">
						 <div class="modal-header">
							 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
								 <span aria-hidden="true">&times;</span></button>
							 <h4 class="modal-title">Update Customer</h4>
						 </div>
						 	<form  onSubmit={this.handleSubmit.bind(this)} >
						 <div class="modal-body">
							 <div className="form-group">
 									 <label>Customer Name</label>
 									 <input type="text" id="updcustomername" ref="updcustomername" className="form-control" placeholder="Enter Customer Name"/>
 								 </div>
								 <input type="hidden" id="updcustomerid" ref="updcustomerid" className="form-control" placeholder="Enter Customer Name"/>

 						<div className="form-group">
 								 <label>Customer Address</label>
 								 <input type="text" id="updcustomeraddress" ref="updcustomeraddress" className="form-control" placeholder="Enter Customer Address"/>
 							 </div>
 							 <div className="form-group">
 										<label>Customer Contact #</label>
 										<input type="text" id="updcustomercontactnumber" ref="updcustomercontactnumber" className="form-control" placeholder="Enter Customer Contact #"/>
 									</div>

										 <div className="form-group">
													<label>Customer Password</label>
													<input type="text" id="updcustomerpassword" ref="updcustomerpassword" className="form-control" placeholder="Enter Password if you want to change"/>
												</div>
						 </div>
						 <div class="modal-footer">
							 <button type="button" id="updmodalclose" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
							 <button type="submit" class="btn btn-primary" >Save changes</button>
						 </div>
					 </form>
					 </div>

				 </div>

			 </div>

			</div>

		)
	}

}
