import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import {DeliveryBoys} from '../../api/collections'
import DeleteDeliveryBoy from './DeleteDeliveryBoy'
import UpdateDeliveryBoyButton from './UpdateDeliveryBoyButton'

import TrackerReact from 'meteor/ultimatejs:tracker-react';


export default class DeliveryBoysList extends TrackerReact(React.Component) {

	constructor(props){
		super(props)
		this.state = {
          subscription: {
            deliveryboys: Meteor.subscribe('deliveryboys')
          }
        }
	}

	componentWillUnmount() {
		this.state.subscription.deliveryboys.stop();
	}

	deliveryboys(){
		return DeliveryBoys.find().fetch();
	}

	handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
    const id = ReactDOM.findDOMNode(this.refs.id).value.trim();

    console.log(id);

  }

	render() {
		let deliveryboys = this.deliveryboys();


		return (
			<div className="box box-info ">
	            <div className="box-header">
	              <h3 className="box-title">Delivery Boys</h3>

	            </div>

	            <div className="box-body table-responsive no-padding">
	              <table className="table table-hover">
	                <tbody>
										<tr>

	                  <th>Name</th>
	                  <th>Address</th>
										<th>Contact #</th>
										<th>Update</th>
										<th>Delete</th>
	                </tr>
									{deliveryboys.map(function(deliveryboy, index){
									return ( <tr key={ index }>
 	                  <td>{deliveryboy.name}</td>
 	                  <td>{deliveryboy.address}</td>
										<td>{deliveryboy.contactnumber}</td>
										<td>
											<UpdateDeliveryBoyButton id={deliveryboy._id} />
										</td>
 	                  <td>
											<DeleteDeliveryBoy id={deliveryboy._id} />
										</td>
 	                </tr>);
									})}


	              </tbody></table>
	            </div>

	          </div>

		)
	}

}
