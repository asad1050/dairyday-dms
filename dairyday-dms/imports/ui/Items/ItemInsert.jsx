import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import swal from 'sweetalert'


export default class ItemInsert extends Component {
	constructor(props){
		super(props)
	}

	handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
    const itemname = ReactDOM.findDOMNode(this.refs.itemname).value.trim();
		const itemprice = ReactDOM.findDOMNode(this.refs.itemprice).value.trim();
		const itemcost = ReactDOM.findDOMNode(this.refs.itemcost).value.trim();

		Meteor.call('createItem',itemname,itemprice,itemcost,(err,res)=>{
			if (err){
				swal("Error", err.message, "error");
			}else{
			swal("Success", "Item successfully created.", "success");
			// Clear form
	    ReactDOM.findDOMNode(this.refs.itemname).value = '';
			ReactDOM.findDOMNode(this.refs.itemprice).value = '';
			ReactDOM.findDOMNode(this.refs.itemcost).value = '';

			}
		})


  }

	render() {


		return (
			<div className="box box-success">
		        <div className="box-header with-border">
		          <h3 className="box-title">Insert Item</h3>


		        </div>
	<form className="new-task" onSubmit={this.handleSubmit.bind(this)} >
		        <div className="box-body">

							<div class="row">
            <div class="col-md-6">
							<div className="form-group">
									 <label>Item Name</label>
									 <input type="text" ref="itemname" className="form-control" placeholder="Enter Item Name"/>
								 </div>
								 <div className="form-group">
	 									 <label>Item Cost</label>
	 									 <input type="text" ref="itemcost" className="form-control" placeholder="Enter Item Cost"/>
	 								 </div>
						</div>
						<div class="col-md-6">
							<div className="form-group">
									 <label>Item Price</label>
									 <input type="text" ref="itemprice" className="form-control" placeholder="Enter Item Price"/>
								 </div>
						</div>
					</div>



							</div>




								<div class="box-footer">

		                 <button type="submit" class="btn btn-success pull-right">Create</button>
		               </div>
									 </form>
		      </div>

		)
	}

}
