import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import {Customers} from '../../api/collections'

import TrackerReact from 'meteor/ultimatejs:tracker-react';


export default class CustomersBalanceReport extends TrackerReact(React.Component) {

	constructor(props){
		super(props)
		this.state = {
          subscription: {
						customers: Meteor.subscribe('customers')
          }
        }
	}

	componentWillUnmount() {
		this.state.subscription.customers.stop();
	}

	customers(){
		return Customers.find().fetch();
	}


	render() {
		let deliveries = this.state.deliveries;
		let customers = this.customers();
		return (
			<div>



			<div className="box box-info">
	            <div className="box-header">
	              <h3 className="box-title">Customers Accounts</h3>

	            </div>



							<div className="box-body table-responsive no-padding">
								<table className="table table-hover">
 								 <tbody>
 									 <tr>

 									 <th>Name</th>
 									 <th>Address</th>
 									 <th>Contact #</th>
 									 <th>Balance</th>
 								 </tr>
 								 {customers.map(function(customer, index){
 								 return ( <tr key={ index }>
 									 <td>{customer.name}</td>
 									 <td>{customer.address}</td>
 									 <td>{customer.contactnumber}</td>
 									 <td>{customer.balance}</td>
 								 </tr>);
 								 })}


 							 </tbody></table>
						 </div>


	          </div>
						</div>
		)
	}

}
