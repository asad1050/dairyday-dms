import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import {Shopkeepers} from '../../api/collections'
import DeleteShopkeeper from './DeleteShopkeeper'
import UpdateShopkeeperButton from './UpdateShopkeeperButton'

import TrackerReact from 'meteor/ultimatejs:tracker-react';


export default class ShopkeepersList extends TrackerReact(React.Component) {

	constructor(props){
		super(props)
		this.state = {
          subscription: {
            shopkeepers: Meteor.subscribe('shopkeepers')
          }
        }
	}

	componentWillUnmount() {
		this.state.subscription.shopkeepers.stop();
	}

	shopkeepers(){
		return Shopkeepers.find().fetch();
	}

	handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
    const id = ReactDOM.findDOMNode(this.refs.id).value.trim();

    console.log(id);

  }

	render() {
		let shopkeepers = this.shopkeepers();


		return (
			<div className="box box-info">
	            <div className="box-header">
	              <h3 className="box-title">Shopkeepers</h3>

	            </div>

	            <div className="box-body table-responsive no-padding">
	              <table className="table table-hover">
	                <tbody>
										<tr>

	                  <th>Name</th>
	                  <th>Address</th>
										<th>Contact #</th>
										<th>Update</th>
										<th>Delete</th>
	                </tr>
									{shopkeepers.map(function(shopkeeper, index){
									return ( <tr key={ index }>
 	                  <td>{shopkeeper.name}</td>
 	                  <td>{shopkeeper.address}</td>
										<td>{shopkeeper.contactnumber}</td>
										<td>
											<UpdateShopkeeperButton id={shopkeeper._id} />
										</td>
 	                  <td>
											<DeleteShopkeeper id={shopkeeper._id} />
										</td>
 	                </tr>);
									})}


	              </tbody></table>
	            </div>

	          </div>

		)
	}

}
