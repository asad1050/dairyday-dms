import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import {Deliveries,Expenses,PointOfSales} from '../../api/collections'
import $ from 'jquery';
import moment from 'moment';
import 'bootstrap-datepicker';

import TrackerReact from 'meteor/ultimatejs:tracker-react';


export default class ProfitLossReport extends TrackerReact(React.Component) {

	constructor(props){
		super(props)
		this.state = {
          subscription: {
            deliveries: Meteor.subscribe('deliveries'),
						expenses: Meteor.subscribe('expenses'),
						pointofsales: Meteor.subscribe('pointofsales')
          },
					data:[],
					totalprice:0,
					totalprofit:0,
					totalexpenses:0,
					netvalue:0
        }
	}

	componentWillUnmount() {
		this.state.subscription.deliveries.stop();
		this.state.subscription.expenses.stop();
		this.state.subscription.pointofsales.stop();
	}

	componentDidMount(){

		$('.datepicker').datepicker({
			format: 'mm-dd-yyyy',
        clearButton: true,
        weekStart: 1,
        time: false,
				autoclose: true
    });
	}


	dateformat(date){
		return moment(date).format('MMMM Do YYYY');
	}

	handleSubmit(event) {
    event.preventDefault();
		const datefrom = ReactDOM.findDOMNode(this.refs.datefrom).value.trim();
		const dateto = ReactDOM.findDOMNode(this.refs.dateto).value.trim();
		var datef = new Date(datefrom);
		var datet = new Date(dateto);
		datef = datef.toISOString();
		datet = datet.toISOString();
		var deliveries = Deliveries.find({'date' : { $gte : datef, $lt: datet }}).fetch();
		var pointofsales = PointOfSales.find({'date' : { $gte : datef, $lt: datet }}).fetch();
		var expenses = Expenses.find({'date' : { $gte : datef, $lt: datet }}).fetch();
		console.log(deliveries);

		var data = [];
		var totalprice = 0;
		var totalprofit = 0;
		var totalexpenses = 0;
		var netvalue = 0;
		for (var i=0;i<deliveries.length;i++){
			data.push({ 'type':"Delivery", 'date':this.dateformat(deliveries[i].date),'profit': deliveries[i].profit,'totalprice':deliveries[i].totalprice });
			totalprice = parseFloat(totalprice) + parseFloat(deliveries[i].totalprice);
			totalprofit = parseFloat(totalprofit) + parseFloat(deliveries[i].profit);
		}
		for (var i=0;i<pointofsales.length;i++){
			data.push({ 'type':"Point Of Sale", 'date':this.dateformat(pointofsales[i].date),'profit': pointofsales[i].profit,'totalprice':pointofsales[i].totalprice });
			totalprice = parseFloat(totalprice) + parseFloat(pointofsales[i].totalprice);
			totalprofit = parseFloat(totalprofit) + parseFloat(pointofsales[i].profit);
		}
		for (var i=0;i<expenses.length;i++){
			data.push({ 'type':"Expense",'name':expenses[i].name, 'date':this.dateformat(expenses[i].date),'profit': expenses[i].profit,'amount':expenses[i].amount });
			totalexpenses = parseFloat(totalexpenses) + parseFloat(expenses[i].amount);

		}

		netvalue = parseFloat(totalprofit) - parseFloat(totalexpenses);
		console.log(data);
		console.log(totalprice);
		console.log(totalprofit);
		console.log(totalexpenses);
		console.log(netvalue);
		this.setState({
			data:data,
			totalprice:totalprice,
			totalprofit:totalprofit,
			totalexpenses:totalexpenses,
			netvalue:netvalue
		 });

  }

	render() {
		let datas = this.state.data;
		let totalprofit = this.state.totalprofit;
		let totalprice = this.state.totalprice;
		let totalexpenses = this.state.totalexpenses;
		let netvalue = this.state.netvalue;
		const divStyle = {
		  paddingBottom: '50px'
		};
		return (
			<div>

				<div className="box box-info" >
								<div className="box-header">
									<h3 className="box-title">Information</h3>

								</div>

								<div className="box-body" style={divStyle}>
									<p>Select two dates to get you deliveries between two dates.<br></br> Thanks!</p>
								</div>




							</div>




			<div className="box box-info">
	            <div className="box-header">
	              <h3 className="box-title">Deliveries</h3>

	            </div>

							<div className="box-body" >
								<div class="row">
										<form className="new-task" onSubmit={this.handleSubmit.bind(this)} >
            <div class="col-md-6">
								<div className="form-group">

																<label>Date From:</label>

																<div className="input-group date">
																	<div className="input-group-addon">
																		<i className="fa fa-calendar"></i>
																	</div>
																	<input type="text" ref="datefrom" className="form-control pull-right datepicker" />
																</div>

															</div>


														</div>

														<div class="col-md-6">
																<div className="form-group">

																								<label>Date To:</label>

																								<div className="input-group date">
																									<div className="input-group-addon">
																										<i className="fa fa-calendar"></i>
																									</div>
																									<input type="text" ref="dateto" className="form-control pull-right datepicker" />
																								</div>

																							</div>


																						</div>
																							<div class="col-md-12">
<div className="form-group">
																						<button type="submit" class="btn btn-info pull-right">Retrive</button>
</div>
</div>
																					</form>
													</div>
							</div>

							<div className="box-body table-responsive no-padding">
							 <table className="table table-hover">
								 <tbody>
									 <tr>

									 <th>Type</th>
									 <th>Date</th>
									 <th>Profit</th>
									 <th>Total Price</th>
									 <th>Expense</th>
								 </tr>
								 {datas.map(function(data, index){
								 return ( <tr key={ index }>
										 <td>{data.name}   {data.type}</td>
									 <td>{data.date}</td>
									  <td>{data.profit}</td>
										 <td>{data.totalprice}</td>
										 <td>{data.amount}</td>
									 </tr>);
								 })}

									 <tr>
 								 <td>  	<i className="fa fa-align-justify"></i>  </td>
 							 <td>	<i className="fa fa-align-justify"></i></td>
 								 <td> 	<i className="fa fa-align-justify"></i></td>
 									 <td>	<i className="fa fa-align-justify"></i> </td>
									  <td>	<i className="fa fa-align-justify"></i> </td>
 									</tr>

									<tr>
								 <td>  Total Price:  </td>
							 <td></td>
								 <td> </td>
										<td>{this.state.totalprice}</td>
										 <td> </td>
									</tr>

									<tr>
								 <td>  Total Profit:  </td>
							 <td></td>

										<td>{this.state.totalprofit}</td>
											 <td> </td>
											 <td> </td>
									</tr>

									<tr>
								 <td>  Total Expenses:  </td>
							 <td></td>
								 <td> </td>
								  <td> </td>
										<td>{this.state.totalexpenses}</td>
									</tr>
									<tr>
								 <td>  	<i className="fa fa-align-justify"></i>  </td>
								 <td>	<i className="fa fa-align-justify"></i></td>
								 <td> 	<i className="fa fa-align-justify"></i></td>
								  <td>	<i className="fa fa-align-justify"></i> </td>
								 	<td>	<i className="fa fa-align-justify"></i> </td>
								 </tr>
									<tr>
								 <td>  Net Value:  </td>
							 <td></td>
								 <td> </td>
										<td>{this.state.netvalue}</td>
									</tr>


							 </tbody></table>
						 </div>


	          </div>
						</div>
		)
	}

}
