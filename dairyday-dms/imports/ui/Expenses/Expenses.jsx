import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import swal from 'sweetalert'
import TrackerReact from 'meteor/ultimatejs:tracker-react';


export default class Expenses extends Component {
	constructor(props){
		super(props)
		this.state = {}
	}
	handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
		const name = ReactDOM.findDOMNode(this.refs.name).value.trim();
		const amount = ReactDOM.findDOMNode(this.refs.amount).value.trim();
		var date = new Date();
		date = date.toISOString();

		Meteor.call('insertExpense',name,amount, date,(err,res)=>{
			if (err){
				swal("Error", err.message, "error");
			}else{
			swal("Success", "Expense added successfully.", "success");
			// Clear form

			ReactDOM.findDOMNode(this.refs.amount).value = '';

			}
		})


  }

	render() {

		return (
			<div className="box box-success">
		        <div className="box-header with-border">
		          <h3 className="box-title">Insert Expenses</h3>

		        </div>
	<form className="new-task" onSubmit={this.handleSubmit.bind(this)} >
		        <div className="box-body">

							<div class="row">
  <div class="col-md-12">


		<div className="form-group">
				 <label>Name</label>
				 <input type="text" ref="name" className="form-control" placeholder="Enter name of expense"/>
			 </div>


						<div className="form-group">
								 <label>Amount</label>
								 <input type="text" ref="amount" className="form-control" placeholder="Enter amount of expense"/>
							 </div>
</div>
						</div>
					</div>



								<div class="box-footer">

		                 <button type="submit" class="btn btn-success pull-right">Insert</button>
		               </div>
									 </form>
		      </div>

		)
	}

}
