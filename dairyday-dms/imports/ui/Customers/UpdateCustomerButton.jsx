import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import {Customers} from '../../api/collections'

export default class UpdateCustomerButton extends Component {

	constructor(props){
		super(props)
		this.state = {
          subscription: {
            customers: Meteor.subscribe('customers')
          }
        }
	}

	componentWillUnmount() {
		this.state.subscription.customers.stop();
	}

	handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
    const id = ReactDOM.findDOMNode(this.refs.id).value.trim();
		var customer = Customers.findOne({'_id':id});
		document.getElementById("updcustomername").value = customer.name;
		document.getElementById("updcustomeraddress").value = customer.address;
		document.getElementById("updcustomercontactnumber").value = customer.contactnumber;
		document.getElementById("updcustomerid").value = id;

  }

	render() {

		return (
			<div>
				<form  onSubmit={this.handleSubmit.bind(this)} >
					<input type="hidden" value={this.props.id} ref="id" />
					<button type="submit"  class="btn btn-default btn-block" data-toggle="modal" data-target="#modal-default">
												<i class="fa fa-edit"></i>
											 </button>
				</form>
			</div>

		)
	}

}
