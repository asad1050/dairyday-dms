import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import {DeliveryBoyStock,Items,DeliveryBoys} from '../../api/collections'
import RefreshItem from './RefreshItem'

import TrackerReact from 'meteor/ultimatejs:tracker-react';


export default class StockInList extends TrackerReact(React.Component) {

	constructor(props){
		super(props)
		this.state = {
          subscription: {
						items: Meteor.subscribe('items'),
            deliveryboystock: Meteor.subscribe('deliveryboystock'),
						deliveryboys: Meteor.subscribe('deliveryboys')

          }
        }
	}

	componentWillUnmount() {
		this.state.subscription.deliveryboystock.stop();
		this.state.subscription.items.stop();
		this.state.subscription.deliveryboys.stop();
	}


	deliveryboystock(){
		var deliveries = DeliveryBoyStock.find().fetch();

		var ndeliveries = deliveries;
		for (var i=0;i<deliveries.length;i++){
			var enitem = this.item(deliveries[i].itemid);
			ndeliveries[i].itemid = enitem.name;
			ndeliveries[i].deliveryboyid = this.deliveryboy(deliveries[i].deliveryboyid);
		}
		return ndeliveries;
	}

	deliveryboy(id){
		return DeliveryBoys.findOne({ '_id':id }).name;
	}
	item(id){
		return Items.findOne({ '_id':id });
	}

	render() {
		let deliveryboystocks = this.deliveryboystock();


		return (
			<div className="box box-info">
	            <div className="box-header">
	              <h3 className="box-title">Delivery Boy Stocks Quantity</h3>

	            </div>

	            <div className="box-body table-responsive no-padding">
	              <table className="table table-hover">
	                <tbody>
										<tr>
											<th>DeliveryBoy</th>
	                  <th>Name</th>

										<th>Quantity</th>

											<th>Refresh</th>
	                </tr>
									{deliveryboystocks.map(function(deliveryboystock, index){
									return ( <tr key={ index }>
 	                  <td>{deliveryboystock.deliveryboyid}</td>
 	                  <td>{deliveryboystock.itemid}</td>
										<td>{deliveryboystock.quantity}</td>
										<td>
											<RefreshItem id={deliveryboystock._id} />
										</td>

 	                </tr>);
									})}


	              </tbody></table>
	            </div>

	          </div>

		)
	}

}
