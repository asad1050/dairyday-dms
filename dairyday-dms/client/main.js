import React from 'react'
import { Meteor } from 'meteor/meteor'
import { render } from 'react-dom'

import '../imports/routes'
import '../imports/api/collections'

Meteor.startup(() => {

})
