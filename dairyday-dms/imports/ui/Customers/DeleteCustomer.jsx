import React, {Component} from 'react'
import ReactDOM from 'react-dom'


export default class DeleteCustomer extends Component {

	constructor(props){
		super(props)

	}

	handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
    const id = ReactDOM.findDOMNode(this.refs.id).value.trim();

		Meteor.call('deleteCustomer',id,(err,res)=>{
			if (err){
				swal("Error", err.message, "error");
			}else{
			swal("Success", "Customer successfully deleted.", "success");

			}
		})

  }

	render() {

		return (
			<div>
				<form  onSubmit={this.handleSubmit.bind(this)} >
					<input type="hidden" value={this.props.id} ref="id" />
				 <button type="submit" class="btn btn-danger  btn-block"><i class="fa fa-trash-o"></i></button>
				</form>
			</div>

		)
	}

}
