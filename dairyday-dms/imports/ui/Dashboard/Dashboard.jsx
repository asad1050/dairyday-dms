import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import {DeliveryBoys,Customers, Deliveries, PointOfSales} from '../../api/collections'

import TrackerReact from 'meteor/ultimatejs:tracker-react';


export default class Dashboard extends TrackerReact(React.Component) {

	constructor(props){
		super(props)
		this.state = {
          subscription: {
            deliveryboys: Meteor.subscribe('deliveryboys'),
						customers: Meteor.subscribe('customers'),
						deliveries: Meteor.subscribe('deliveries'),
						pointofsales: Meteor.subscribe('pointofsales')
          }
        }
	}

	componentWillUnmount() {
		this.state.subscription.deliveryboys.stop();
		this.state.subscription.customers.stop();
		this.state.subscription.deliveries.stop();
		this.state.subscription.pointofsales.stop();
	}

	deliveryboys(){
		return DeliveryBoys.find().fetch();
	}
	cardsshop(){
		var date = new Date();

		var datet = new Date();
		datet.setDate(date.getDate()+1);
		date.setDate(date.getDate());
		date.setHours(0,0,0,0);
		datet.setHours(0,0,0,0);
		date = date.toISOString();
		datet = datet.toISOString();
		PointOfSales.find().count();
		var pointofsales = PointOfSales.find({'date' : { $gte : date, $lt: datet }}).fetch();
		var shopsales = 0;
		var profitshop = 0;
		for (var i=0;i<pointofsales.length;i++){
			shopsales = parseFloat(shopsales) + parseFloat(pointofsales[i].totalprice);
			profitshop = parseFloat(profitshop) + parseFloat(pointofsales[i].profit);
		}
		profitshop = profitshop.toFixed(2);

		var data = [{ 'shopsales':shopsales,'shopprofit':profitshop}];
		return data;
	}

	cardsdelivery(){
		var date = new Date();

		var datet = new Date();
		datet.setDate(date.getDate()+1);
		date.setDate(date.getDate());

		date.setHours(0,0,0,0);
		datet.setHours(0,0,0,0);
		console.log(date);
		console.log(datet);
		date = date.toISOString();
		datet = datet.toISOString();
		Deliveries.find().count();
		var deliveries = Deliveries.find({'date' : { $gte : date, $lt: datet }}).fetch();
		var deliverysales = 0;
		var profitdelivery = 0;
		for (var i=0;i<deliveries.length;i++){
			deliverysales = parseFloat(deliverysales) + parseFloat(deliveries[i].totalprice);
			profitdelivery = parseFloat(profitdelivery) + parseFloat(deliveries[i].profit);
		}
		profitdelivery = profitdelivery.toFixed(2);
		var data = [{ 'deliverysales':deliverysales,'deliveryprofit':profitdelivery }];
		return data;
	}
	customer(){
		if (Meteor.userId()){
			if(Customers.findOne({ 'userid':Meteor.userId() })){
		var customer = Customers.findOne({ 'userid':Meteor.userId() });
		console.log(Meteor.userId());
		console.log(customer);
		if (customer){
			return customer;
		}
	}
	}
	}

	handleSubmit(event) {
    event.preventDefault();

		Meteor.call('atDairyDay',Meteor.userId(),(err,res)=>{
			if (err){
				swal("Error", err.message, "error");
			}else{
			swal("Success", "Location successfully updated.", "success");
			}
		})

  }

	render() {
		let deliveryboys = this.deliveryboys();
		let customer = this.customer();
		let balance = "Loading...";
		let cardsshop = this.cardsshop();
		let cardsdelivery = this.cardsdelivery();
		if (customer){
		 balance = customer.balance;
	}else{
		balance = "Loading...";
	}
		let type = this.props.type;

		return (

<div>
							{(() => {
				        switch (type) {
				          case "deliveryboy":   return (
										<div className="box box-info">
														<div className="box-header">
															<h3 className="box-title">Delivery Boy Location</h3>

														</div>
										<div className="box-body">
											  <form onSubmit={this.handleSubmit.bind(this)} >

											  <button type="submit" className="btn btn-block btn-bitbucket ">Arrived at DairyDay</button>
										</form>
									</div>
									</div>
									);
									case "customer":   return (
										<div className="box box-info">
														<div className="box-header">
															<h3 className="box-title">Delivery Boy Location</h3>

														</div>
										<div>

											<div className="box-body table-responsive no-padding">
											 <table className="table table-hover">
												 <tbody>
													 <tr>

													 <th>Name</th>
													 <th>Contact #</th>
													 <th>Current Location</th>
												 </tr>
												 {deliveryboys.map(function(deliveryboy, index){
												 return ( <tr key={ index }>
														 <td>{deliveryboy.name}</td>
													 <td>{deliveryboy.contactnumber}</td>
														 <td>{deliveryboy.currentlocation}</td>
													 </tr>);
												 })}


											 </tbody></table>
										 </div>



										<div className="box-body">
											<h1>Amount Payable: {balance}</h1>
									</div>

								</div>
								</div>
									);
									case "admin":   return (

										<div>


																						<div className="row">
											        <div className="col-lg-3 col-xs-6">

											          <div className="small-box bg-aqua">
											            <div className="inner">
											              <h3>{cardsshop[0].shopsales}</h3>

											              <p>Shop Cash Sales</p>
											            </div>
											            <div className="icon">
											              <i className="ion ion-bag"></i>
											            </div>

											          </div>
											        </div>

											        <div className="col-lg-3 col-xs-6">

											          <div className="small-box bg-green">
											            <div className="inner">
											              <h3>{cardsshop[0].shopprofit}</h3>

											              <p>Shop Cash Profit</p>
											            </div>
											            <div className="icon">
											              <i className="ion ion-stats-bars"></i>
											            </div>

											          </div>
											        </div>

											        <div className="col-lg-3 col-xs-6">

											          <div className="small-box bg-yellow">
											            <div className="inner">
											              <h3>{cardsdelivery[0].deliverysales}</h3>

											            <p>Delivery Cash</p>
											            </div>
											            <div className="icon">
											              <i className="fa fa-truck"></i>
											            </div>

											          </div>
											        </div>

											        <div className="col-lg-3 col-xs-6">

											          <div className="small-box bg-red">
											            <div className="inner">
											              <h3>{cardsdelivery[0].deliveryprofit}</h3>

											              <p>Delivery Profit</p>
											            </div>
											            <div className="icon">
											              <i className="ion ion-pie-graph"></i>
											            </div>

											          </div>
											        </div>

											      </div>



										<div className="box box-info">
														<div className="box-header">
															<h3 className="box-title">Delivery Boy Location</h3>

														</div>
										<div>


											<div className="box-body table-responsive no-padding">
											 <table className="table table-hover">
												 <tbody>
													 <tr>

													 <th>Name</th>
													 <th>Contact #</th>
													 <th>Current Location</th>
												 </tr>
												 {deliveryboys.map(function(deliveryboy, index){
												 return ( <tr key={ index }>
														 <td>{deliveryboy.name}</td>
													 <td>{deliveryboy.contactnumber}</td>
														 <td>{deliveryboy.currentlocation}</td>
													 </tr>);
												 })}


											 </tbody></table>
										 </div>


								</div>
								</div>
							</div>
									);
				          default:      return (
										<div className="box box-info">
														<div className="box-header">
															<h3 className="box-title">Delivery Boy Location</h3>

														</div>

	            <div className="box-body table-responsive no-padding">
	              <table className="table table-hover">
	                <tbody>
										<tr>

	                  <th>Name</th>
										<th>Contact #</th>
	                  <th>Current Location</th>
	                </tr>
									{deliveryboys.map(function(deliveryboy, index){
									return ( <tr key={ index }>
 	                  <td>{deliveryboy.name}</td>
										<td>{deliveryboy.contactnumber}</td>
 	                  <td>{deliveryboy.currentlocation}</td>
 	                </tr>);
									})}


	              </tbody></table>
	            </div>
							</div>
						);
					}
				})()}


</div>

		)
	}

}
