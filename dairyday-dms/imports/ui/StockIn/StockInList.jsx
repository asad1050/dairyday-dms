import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import {Items} from '../../api/collections'
import RefreshItem from './RefreshItem'

import TrackerReact from 'meteor/ultimatejs:tracker-react';


export default class StockInList extends TrackerReact(React.Component) {

	constructor(props){
		super(props)
		this.state = {
          subscription: {
            items: Meteor.subscribe('items')
          }
        }
	}

	componentWillUnmount() {
		this.state.subscription.items.stop();
	}

	items(){
		return Items.find().fetch();
	}

	render() {
		let items = this.items();


		return (
			<div className="box box-info">
	            <div className="box-header">
	              <h3 className="box-title">Items Quantity</h3>

	            </div>

	            <div className="box-body table-responsive no-padding">
	              <table className="table table-hover">
	                <tbody>
										<tr>

	                  <th>Name</th>
	                  <th>Price</th>
										<th>Quantity</th>
											<th>Refresh</th>
	                </tr>
									{items.map(function(item, index){
									return ( <tr key={ index }>
 	                  <td>{item.name}</td>
 	                  <td>{item.price}</td>
										<td>{item.quantity}</td>
										<td>
											<RefreshItem id={item._id} />
										</td>

 	                </tr>);
									})}


	              </tbody></table>
	            </div>

	          </div>

		)
	}

}
