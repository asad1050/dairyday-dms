import React, {Component} from 'react'
import ReactDOM from 'react-dom'


export default class App extends Component {
	constructor(props){
		super(props)

	}

	handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
    const text = ReactDOM.findDOMNode(this.refs.textInput).value.trim();

    console.log(text);

    // Clear form
    ReactDOM.findDOMNode(this.refs.textInput).value = '';
  }

	render() {
		

		return (
		<div>
			<form className="new-task" onSubmit={this.handleSubmit.bind(this)} >
					<input
						type="text"
						ref="textInput"
						placeholder="Type to add new tasks"
					/>
				</form>
		</div>

		)
	}

}
