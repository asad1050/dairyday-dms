import React, {Component} from 'react'
import ReactDOM from 'react-dom'


export default class UpdateItem extends Component {

	constructor(props){
		super(props)

	}

	handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
		const itemname = ReactDOM.findDOMNode(this.refs.upditemname).value.trim();
		const itemprice = ReactDOM.findDOMNode(this.refs.upditemprice).value.trim();
		const itemcost = ReactDOM.findDOMNode(this.refs.upditemcost).value.trim();
		const itemid = ReactDOM.findDOMNode(this.refs.upditemid).value.trim();
		Meteor.call('updateItem',itemid, itemname,itemprice,itemcost,(err,res)=>{
			if (err){
				swal("Error", err.message, "error");
			}else{
			swal("Success", "Item successfully Updated.", "success");
			document.getElementById("updmodalclose").click();
			ReactDOM.findDOMNode(this.refs.upditemname).value = '';
			ReactDOM.findDOMNode(this.refs.upditemprice).value = '';
			ReactDOM.findDOMNode(this.refs.upditempcost).value = '';
			}
		})

  }

	render() {

		return (
			<div>
				<div class="modal fade" id="modal-default">
				 <div class="modal-dialog">
					 <div class="modal-content">
						 <div class="modal-header">
							 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
								 <span aria-hidden="true">&times;</span></button>
							 <h4 class="modal-title">Update Item</h4>
						 </div>
						 	<form  onSubmit={this.handleSubmit.bind(this)} >
						 <div class="modal-body">
							 <div className="form-group">
 									 <label>Item Name</label>
 									 <input type="text" id="upditemname" ref="upditemname" className="form-control" placeholder="Enter Item Name"/>
 								 </div>
								 <input type="hidden" id="upditemid" ref="upditemid" className="form-control" placeholder="Enter Item Name"/>

 						<div className="form-group">
 								 <label>Item Price</label>
 								 <input type="text" id="upditemprice" ref="upditemprice" className="form-control" placeholder="Enter Item Price"/>
 							 </div>
							 <div className="form-group">
											<label>Item Cost</label>
											<input type="text" id="upditemcost" ref="upditemcost" className="form-control" placeholder="Enter Item COst"/>
										</div>

						 </div>
						 <div class="modal-footer">
							 <button type="button" id="updmodalclose" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
							 <button type="submit" class="btn btn-primary" >Save changes</button>
						 </div>
					 </form>
					 </div>

				 </div>

			 </div>

			</div>

		)
	}

}
