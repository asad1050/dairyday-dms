import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import {DeliveryBoys} from '../../api/collections'

export default class UpdateDeliveryBoyButton extends Component {

	constructor(props){
		super(props)
		this.state = {
          subscription: {
            deliveryboys: Meteor.subscribe('deliveryboys')
          }
        }
	}

	componentWillUnmount() {
		this.state.subscription.deliveryboys.stop();
	}

	handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
    const id = ReactDOM.findDOMNode(this.refs.id).value.trim();
		var deliveryboy = DeliveryBoys.findOne({'_id':id});
		document.getElementById("upddeliveryboyname").value = deliveryboy.name;
		document.getElementById("upddeliveryboyaddress").value = deliveryboy.address;
		document.getElementById("upddeliveryboycontactnumber").value = deliveryboy.contactnumber;
		document.getElementById("upddeliveryboyid").value = id;

  }

	render() {

		return (
			<div>
				<form  onSubmit={this.handleSubmit.bind(this)} >
					<input type="hidden" value={this.props.id} ref="id" />
					<button type="submit"  class="btn btn-default btn-block" data-toggle="modal" data-target="#modal-default">
												<i class="fa fa-edit"></i>
											 </button>
				</form>
			</div>

		)
	}

}
