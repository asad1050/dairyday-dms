import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import {Customers} from '../../api/collections'
import DeleteCustomer from './DeleteCustomer'
import UpdateCustomerButton from './UpdateCustomerButton'

import TrackerReact from 'meteor/ultimatejs:tracker-react';


export default class CustomersList extends TrackerReact(React.Component) {

	constructor(props){
		super(props)
		this.state = {
          subscription: {
            customers: Meteor.subscribe('customers')
          }
        }
	}

	componentWillUnmount() {
		this.state.subscription.customers.stop();
	}

	customers(){
		return Customers.find().fetch();
	}

	handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
    const id = ReactDOM.findDOMNode(this.refs.id).value.trim();

    console.log(id);

  }

	render() {
		let customers = this.customers();


		return (
			<div className="box box-info">
	            <div className="box-header">
	              <h3 className="box-title">Customers</h3>

	            </div>

	            <div className="box-body table-responsive no-padding">
	              <table className="table table-hover">
	                <tbody>
										<tr>

	                  <th>Name</th>
	                  <th>Address</th>
										<th>Contact #</th>
										<th>Update</th>
										<th>Delete</th>
	                </tr>
									{customers.map(function(customer, index){
									return ( <tr key={ index }>
 	                  <td>{customer.name}</td>
 	                  <td>{customer.address}</td>
										<td>{customer.contactnumber}</td>
										<td>
											<UpdateCustomerButton id={customer._id} />
										</td>
 	                  <td>
											<DeleteCustomer id={customer._id} />
										</td>
 	                </tr>);
									})}


	              </tbody></table>
	            </div>

	          </div>

		)
	}

}
