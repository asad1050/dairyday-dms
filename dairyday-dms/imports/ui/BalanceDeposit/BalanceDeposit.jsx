import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import swal from 'sweetalert'
import {Customers,DeliveryBoys} from '../../api/collections'
import TrackerReact from 'meteor/ultimatejs:tracker-react';


export default class BalanceDeposit extends TrackerReact(React.Component) {
	constructor(props){
		super(props)
		this.state = {
          subscription: {
						customers: Meteor.subscribe('customers'),
						deliveryboys: Meteor.subscribe('deliveryboys')
          }
        }
	}

	componentWillUnmount() {
		this.state.subscription.customers.stop();
		this.state.subscription.deliveryboys.stop();
	}

	customers(){
		return Customers.find().fetch();
	}

	deliveryboys(){
		return DeliveryBoys.find().fetch();
	}


	handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
		const customerid = ReactDOM.findDOMNode(this.refs.customerid).value.trim();
		const amount = ReactDOM.findDOMNode(this.refs.amount).value.trim();
		var date = new Date();
		date = date.toISOString();
		var deliveryboyid = DeliveryBoys.findOne({ 'userid':Meteor.userId()})._id;

		Meteor.call('insertDeposit',customerid,amount,deliveryboyid, date,(err,res)=>{
			if (err){
				swal("Error", err.message, "error");
			}else{
			swal("Success", "Payment deposited successfully.", "success");
			// Clear form

			ReactDOM.findDOMNode(this.refs.amount).value = '';

			}
		})


  }

	render() {
		let customers = this.customers();
		return (
			<div className="box box-success">
		        <div className="box-header with-border">
		          <h3 className="box-title">Insert BalanceDeposit</h3>

		        </div>
	<form className="new-task" onSubmit={this.handleSubmit.bind(this)} >
		        <div className="box-body">

							<div class="row">
  <div class="col-md-12">



			<div class="form-group">
					<label>Select Customer</label>
					<select ref="customerid" class="form-control">
						{customers.map(function(customer, index){
						return (<option key={ index } value={customer._id} >{customer.name} - {customer.contactnumber}</option>);
						})}

					</select>
				</div>

						<div className="form-group">
								 <label>Amount</label>
								 <input type="text" ref="amount" className="form-control" placeholder="Enter amount for deposit"/>
							 </div>
</div>
						</div>
					</div>



								<div class="box-footer">

		                 <button type="submit" class="btn btn-success pull-right">Insert</button>
		               </div>
									 </form>
		      </div>

		)
	}

}
