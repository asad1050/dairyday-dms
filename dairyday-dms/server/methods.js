import {Users, StockIn, Items, Customers, Deliveries, DeliveryBoys,Payments,Shopkeepers,PointOfSales,Expenses,DeliveryBoyStock} from '../imports/api/collections'

if (Meteor.isServer){
Meteor.methods({
//Authentication
	'checkAdmin': () =>{
		var type = Users.findOne({ '_id':Meteor.userId() }).type;
		if (type == "admin"){
			return "success";
		}else{
			throw new Meteor.Error("Forbidden");
		}
	},
	'checkCustomer': () =>{
		var type = Users.findOne({ '_id':Meteor.userId() }).type;
		if (type == "customer"){
			return "success";
		}else{
			throw new Meteor.Error("Forbidden");
		}
	},
	'checkDeliveryBoy': () =>{
		var type = Users.findOne({ '_id':Meteor.userId() }).type;
		if (type == "deliveryboy"){
			return "success";
		}else{
			throw new Meteor.Error("Forbidden");
		}
	},
	'checkShopkeeper': () =>{
		var type = Users.findOne({ '_id':Meteor.userId() }).type;
		if (type == "shopkeeper"){
			return "success";
		}else{
			throw new Meteor.Error("Forbidden");
		}
	},
	'checkAdminShopkeeper': () =>{
		var type = Users.findOne({ '_id':Meteor.userId() }).type;
		if (type == "shopkeeper" || type == "admin"){
			return "success";
		}else{
			throw new Meteor.Error("Forbidden");
		}
	},


//Items
	'createItem': (name,price,cost) =>{
		if (name=="" || price == ""){
			throw new Meteor.Error('Blank Inputs');
		}

		if (Items.findOne({'name':name})){
			throw new Meteor.Error("Item with same name already exists");
		}
		return Items.insert({ 'name':name,'price':price,'cost':cost,'quantity':0 });
	},
	'updateItem': (id,name,price,cost) =>{
		if (name=="" || price == "" ){
			throw new Meteor.Error('Blank Inputs');
		}
		return id = Items.update(id,{ $set: { 'name':name,'price':price,'cost':cost}});

	},
	'deleteItem': (id) =>{

		Items.remove(id);
	},
	'refreshItem': (id) =>{

		Items.update(id,{ $set:{ 'quantity':0 } });
	},

//Customers
'createCustomer': (name,address,contactnumber,username,password) =>{
	if (name=="" || address == "" || contactnumber == "" || username == "" || password == ""){
		throw new Meteor.Error('Blank Inputs');
	}

	if (Customers.findOne({'name':name})){
		throw new Meteor.Error("Customer with same name already exists");
	}


	var uid = Accounts.createUser({ 'username':username, 'email':username, 'password':password})
	var id = Customers.insert({ 'name':name,'address':address, 'contactnumber':contactnumber,'balance':0,'userid':uid });
	Users.update(uid,{ $set:{ 'type':"customer" } });
},
'updateCustomer': (id,name,address,contactnumber,password) =>{
	if (name=="" || address == "" || contactnumber == ""){
		throw new Meteor.Error('Blank Inputs');
	}
	var uid = Customers.findOne({'_id':id}).userid;
	if (password == ""){}else{
		Accounts.setPassword(uid,password);
	}

	return id = Customers.update(id,{ $set: { 'name':name,'address':address, 'contactnumber':contactnumber}});

},
'deleteCustomer': (id) =>{

	Users.remove(Customers.findOne({'_id':id}).userid);
	Customers.remove(id);

},

//ChangePassword
'changeUserPassword': (oldpassword,newpassword,confirmpassword) =>{
	if (oldpassword=="" || newpassword == "" || confirmpassword == ""){
		throw new Meteor.Error('Blank Inputs');
	}
	if (newpassword !== confirmpassword){
		throw new Meteor.Error('New password and Confirm password mismatch');
	}
	Accounts.changePassword(oldpassword, newpassword,(err,res)=>{
		if (err){
			throw new Meteor.Error(err.message);
		}
	});

},

//Shopkeepers
'createShopkeeper': (name,address,contactnumber,username,password) =>{
	if (name=="" || address == "" || contactnumber == "" || username == "" || password == ""){
		throw new Meteor.Error('Blank Inputs');
	}

	if (Shopkeepers.findOne({'name':name})){
		throw new Meteor.Error("Shopkeeper with same name already exists");
	}


	var uid = Accounts.createUser({ 'username':username, 'email':username, 'password':password})
	var id = Shopkeepers.insert({ 'name':name,'address':address, 'contactnumber':contactnumber,'balance':0,'userid':uid });
	Users.update(uid,{ $set:{ 'type':"shopkeeper" } });
},
'updateShopkeeper': (id,name,address,contactnumber,password) =>{
	if (name=="" || address == "" || contactnumber == ""){
		throw new Meteor.Error('Blank Inputs');
	}
	var uid = Shopkeepers.findOne({'_id':id}).userid;
	if (password == ""){}else{
		Accounts.setPassword(uid,password);
	}

	return id = Shopkeepers.update(id,{ $set: { 'name':name,'address':address, 'contactnumber':contactnumber}});

},
'deleteShopkeeper': (id) =>{

	Users.remove(Shopkeepers.findOne({'_id':id}).userid);
	Shopkeepers.remove(id);

},

//DeliveryBoys
'createDeliveryBoy': (name,address,contactnumber,username,password) =>{
	if (name=="" || address == "" || contactnumber == "" || username == "" || password == ""){
		throw new Meteor.Error('Blank Inputs');
	}

	if (DeliveryBoys.findOne({'name':name})){
		throw new Meteor.Error("DeliveryBoy with same name already exists");
	}

	var uid = Accounts.createUser({ 'username':username, 'email':username, 'password':password});
	var id = DeliveryBoys.insert({ 'name':name,'address':address, 'contactnumber':contactnumber,'currentlocation':"DairyDay", 'userid':uid });
	Users.update(uid,{ $set:{ 'type':"deliveryboy" } });
},
'updateDeliveryBoy': (id,name,address,contactnumber,password) =>{
	if (name=="" || address == "" || contactnumber == ""){
		throw new Meteor.Error('Blank Inputs');
	}
	var uid = DeliveryBoys.findOne({'_id':id}).userid;
	if (password == ""){}else{
		Accounts.setPassword(uid,password);
	}

	return id = DeliveryBoys.update(id,{ $set: { 'name':name,'address':address, 'contactnumber':contactnumber}});

},
'deleteDeliveryBoy': (id) =>{
	Users.remove(DeliveryBoys.findOne({'_id':id}).userid);
	DeliveryBoys.remove(id);

},

//StockIn
'insertQuantity': (id,quantity,date) =>{
	if (quantity==""){
		throw new Meteor.Error('Blank Inputs');
	}
	quantity = parseFloat(quantity);
	check(quantity,Number);

	var itemquantity = Items.findOne({ '_id':id }).quantity;
	itemquantity = parseFloat(itemquantity);
	itemquantity = itemquantity + quantity;
	StockIn.insert({ 'itemid':id, 'quantity':quantity, 'date':date });
	Items.update(id, {$set: { 'quantity':itemquantity}});

},

//Delivery
'insertDelivery': (iid,cid,did,quantity,date) =>{
	if (quantity==""){
		throw new Meteor.Error('Blank Inputs');
	}
	quantity = parseFloat(quantity);
	check(quantity,Number);

	var item = Items.findOne({ '_id':iid });
	var deliveryboystock = DeliveryBoyStock.findOne({ 'deliveryboyid':did, 'itemid':iid });
	var balance = parseFloat(Customers.findOne({ '_id':cid }).balance);
	var itemprice = parseFloat(item.price);
	var itemcost = parseFloat(item.cost);
	var itemquantity = deliveryboystock.quantity;
	var profit = itemprice - itemcost;
	itemquantity = parseFloat(itemquantity);
	profit = profit * quantity;

	var totalprice = itemprice * quantity;
	balance = balance + totalprice;

	itemquantity = itemquantity - quantity;
	Deliveries.insert({ 'itemid':iid, 'customerid':cid, 'deliveryboyid':did, 'quantity':quantity, 'date':date, 'profit':profit, 'totalprice':totalprice });
	Customers.update(cid,{ $set:{ 'balance':balance } });

	DeliveryBoyStock.update(deliveryboystock._id, {$set: { 'quantity':itemquantity}});

	var location = Customers.findOne({ '_id':cid }).address;
	DeliveryBoys.update(did,{ $set:{ 'currentlocation':location } });

},

//Transaction
'insertTransaction': (cart,totalprice,profit,date) =>{
	if (cart==[] || totalprice == 0 || profit==0){
		throw new Meteor.Error('Blank Inputs');
	}
	totalprice = parseFloat(totalprice);
	profit = parseFloat(profit);
	check(totalprice,Number);
	check(profit,Number);

	for (var i=0;i<cart.length;i++){
		var item = Items.findOne({ '_id':cart[i].itemid });
		var itemquantity = item.quantity;
		itemquantity = parseFloat(itemquantity);
		itemquantity = itemquantity - parseFloat(cart[i].quantity);
			Items.update(cart[i].itemid, {$set: { 'quantity':itemquantity}});
	}

	PointOfSales.insert({ 'cart':cart, 'date':date, 'profit':profit, 'totalprice':totalprice });


},

//DeliveryBoyStockIn
'insertDeliveryBoyStockIn': (iid,did,quantity) =>{
	if (quantity==""){
		throw new Meteor.Error('Blank Inputs');
	}
	if (DeliveryBoyStock.findOne({'deliveryboyid':did, 'itemid':iid})){
		quantity = parseFloat(quantity);
		check(quantity,Number);
		var itemquantity = Items.findOne({ '_id':iid }).quantity;
		itemquantity = parseFloat(itemquantity);
		itemquantity = itemquantity - quantity;
		Items.update(iid, {$set: { 'quantity':itemquantity}});
		var dbstock = DeliveryBoyStock.findOne({'deliveryboyid':did, 'itemid':iid});
		var dbquantity = parseFloat(dbstock.quantity);
		dbquantity = dbquantity + quantity;
	var stockid = DeliveryBoyStock.findOne({'deliveryboyid':did, 'itemid':iid})._id;
	DeliveryBoyStock.update(stockid,{ $set: {'quantity':dbquantity} })
	}else{
		quantity = parseFloat(quantity);
		check(quantity,Number);
		var itemquantity = Items.findOne({ '_id':iid }).quantity;
		itemquantity = parseFloat(itemquantity);
		itemquantity = itemquantity - quantity;
		Items.update(iid, {$set: { 'quantity':itemquantity}});

		DeliveryBoyStock.insert({ 'deliveryboyid':did,'itemid':iid ,'quantity':quantity});
	}


},
'refreshDeliveryBoyStock': (id) =>{

	DeliveryBoyStock.update(id,{ $set:{ 'quantity':0 } });
},

//DepoistBalance
'insertDeposit': (cid,amount,did,date) =>{
	if (amount==""){
		throw new Meteor.Error('Blank Inputs');
	}
	amount = parseFloat(amount);
	check(amount,Number);

	Payments.insert({ 'customerid':cid, 'deliveryboyid':did, 'amount':amount, 'date':date });
	var balance = parseFloat(Customers.findOne({ '_id':cid }).balance);
	balance = balance - amount;

	Customers.update(cid,{ $set:{ 'balance':balance } });
	var location = Customers.findOne({ '_id':cid }).address;
	DeliveryBoys.update(did,{ $set:{ 'currentlocation':location } });

},

//Expenses
'insertExpense': (name,amount,date) =>{
	if (amount=="" || name==""){
		throw new Meteor.Error('Blank Inputs');
	}
	amount = parseFloat(amount);
	check(amount,Number);

	Expenses.insert({ 'name':name, 'amount':amount, 'date':date });


},

	'atDairyDay': (id) => {
		if (id){
			return DeliveryBoys.update({ 'userid':id },{ $set:{ 'currentlocation':"DairyDay" } });
		}
	}


});


}
