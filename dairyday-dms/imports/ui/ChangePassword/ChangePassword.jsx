import React, {Component} from 'react'
import ReactDOM from 'react-dom'


export default class ChangePassword extends Component {

	constructor(props){
		super(props)
	}

  handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
    const oldpassword = ReactDOM.findDOMNode(this.refs.oldpassword).value.trim();
		const newpassword = ReactDOM.findDOMNode(this.refs.newpassword).value.trim();
		const confirmpassword = ReactDOM.findDOMNode(this.refs.confirmpassword).value.trim();

    if (oldpassword=="" || newpassword == "" || confirmpassword == ""){
      swal("Error", 'Blank Inputs', "error");
  	}
  	if (newpassword !== confirmpassword){
      swal("Error", 'New password and Confirm password mismatch', "error");
  	}
  	Accounts.changePassword(oldpassword, newpassword,(err,res)=>{
  		if (err){
        swal("Error", err.message, "error");
  		}else{
			swal("Success", "Password changed successfully.", "success");
			// Clear form
	    ReactDOM.findDOMNode(this.refs.oldpassword).value = '';
			ReactDOM.findDOMNode(this.refs.newpassword).value = '';
			ReactDOM.findDOMNode(this.refs.confirmpassword).value = '';

			}
  	});


  }

	render() {


		return (
			<div className="box box-success">
		        <div className="box-header with-border">
		          <h3 className="box-title">Change Password</h3>

		        </div>
	<form className="new-task" onSubmit={this.handleSubmit.bind(this)} >
		        <div className="box-body">

							<div className="row">
            <div className="col-md-6">
							<div className="form-group">
									 <label>Old Password</label>
									 <input type="password" ref="oldpassword" className="form-control" placeholder="Enter Old Password"/>
								 </div>

							 	</div>

						<div className="col-md-6">
							<div className="form-group">
									 <label>New Password</label>
									 <input type="password" ref="newpassword" className="form-control" placeholder="Enter New Password"/>
								 </div>
								 <div className="form-group">
											<label>Confirm New Password</label>
											<input type="password" ref="confirmpassword" className="form-control" placeholder="Enter Confirm New Password"/>
										</div>
						</div>
						</div>
					</div>



								<div className="box-footer">

		                 <button type="submit" className="btn btn-success pull-right">Change Password</button>
		               </div>
									 </form>
		      </div>

		)
	}

}
