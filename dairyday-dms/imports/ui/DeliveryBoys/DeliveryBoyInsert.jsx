import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import swal from 'sweetalert'


export default class DeliveryBoyInsert extends Component {
	constructor(props){
		super(props)
	}

	handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
    const deliveryboyname = ReactDOM.findDOMNode(this.refs.deliveryboyname).value.trim();
		const deliveryboyaddress = ReactDOM.findDOMNode(this.refs.deliveryboyaddress).value.trim();
		const deliveryboycontactnumber = ReactDOM.findDOMNode(this.refs.deliveryboycontactnumber).value.trim();
		const deliveryboyusername = ReactDOM.findDOMNode(this.refs.deliveryboyusername).value.trim();
		const deliveryboypassword = ReactDOM.findDOMNode(this.refs.deliveryboypassword).value.trim();

		Meteor.call('createDeliveryBoy',deliveryboyname,deliveryboyaddress, deliveryboycontactnumber, deliveryboyusername, deliveryboypassword,(err,res)=>{
			if (err){
				swal("Error", err.message, "error");
			}else{
			swal("Success", "DeliveryBoy successfully created.", "success");
			// Clear form
	    ReactDOM.findDOMNode(this.refs.deliveryboyname).value = '';
			ReactDOM.findDOMNode(this.refs.deliveryboyaddress).value = '';
			ReactDOM.findDOMNode(this.refs.deliveryboycontactnumber).value = '';
			ReactDOM.findDOMNode(this.refs.deliveryboyusername).value = '';
			ReactDOM.findDOMNode(this.refs.deliveryboypassword).value = '';

			}
		})


  }

	render() {


		return (
			<div className="box box-success">
		        <div className="box-header with-border">
		          <h3 className="box-title">Insert DeliveryBoy</h3>

		        </div>
	<form className="new-task" onSubmit={this.handleSubmit.bind(this)} >
		        <div className="box-body">

							<div class="row">
            <div class="col-md-6">
							<div className="form-group">
									 <label>DeliveryBoy Name</label>
									 <input type="text" ref="deliveryboyname" className="form-control" placeholder="Enter DeliveryBoy Name"/>
								 </div>

						<div className="form-group">
								 <label>DeliveryBoy Address</label>
								 <input type="text" ref="deliveryboyaddress" className="form-control" placeholder="Enter DeliveryBoy Address"/>
							 </div>
							 <div className="form-group">
										<label>DeliveryBoy Contact #</label>
										<input type="text" ref="deliveryboycontactnumber" className="form-control" placeholder="Enter Deliveryboy Contact #"/>
									</div>
							 	</div>

						<div class="col-md-6">
							<div className="form-group">
									 <label>DeliveryBoy Username</label>
									 <input type="text" ref="deliveryboyusername" className="form-control" placeholder="Enter DeliveryBoy Username"/>
								 </div>
								 <div className="form-group">
											<label>DeliveryBoy Password</label>
											<input type="text" ref="deliveryboypassword" className="form-control" placeholder="Enter DeliveryBoy Password"/>
										</div>
						</div>
						</div>
					</div>



								<div class="box-footer">

		                 <button type="submit" class="btn btn-success pull-right">Create</button>
		               </div>
									 </form>
		      </div>

		)
	}

}
