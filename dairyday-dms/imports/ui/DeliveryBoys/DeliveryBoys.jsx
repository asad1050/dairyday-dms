import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import DeliveryBoyInsert from './DeliveryBoyInsert'
import DeliveryBoysList from './DeliveryBoysList'
import UpdateDeliveryBoy from './UpdateDeliveryBoy'


export default class DeliveryBoys extends Component {
	constructor(props){
		super(props)
	}


	render() {


		return (
		<div>
		<DeliveryBoyInsert/>
		<DeliveryBoysList/>
		<UpdateDeliveryBoy/>
		</div>

		)
	}

}
