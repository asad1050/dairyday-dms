import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import swal from 'sweetalert'


export default class ShopkeeperInsert extends Component {
	constructor(props){
		super(props)
	}

	handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
    const shopkeepername = ReactDOM.findDOMNode(this.refs.shopkeepername).value.trim();
		const shopkeeperaddress = ReactDOM.findDOMNode(this.refs.shopkeeperaddress).value.trim();
		const shopkeepercontactnumber = ReactDOM.findDOMNode(this.refs.shopkeepercontactnumber).value.trim();
		const shopkeeperusername = ReactDOM.findDOMNode(this.refs.shopkeeperusername).value.trim();
		const shopkeeperpassword = ReactDOM.findDOMNode(this.refs.shopkeeperpassword).value.trim();

		Meteor.call('createShopkeeper',shopkeepername,shopkeeperaddress, shopkeepercontactnumber, shopkeeperusername, shopkeeperpassword,(err,res)=>{
			if (err){
				swal("Error", err.message, "error");
			}else{
			swal("Success", "Shopkeeper successfully created.", "success");
			// Clear form
	    ReactDOM.findDOMNode(this.refs.shopkeepername).value = '';
			ReactDOM.findDOMNode(this.refs.shopkeeperaddress).value = '';
			ReactDOM.findDOMNode(this.refs.shopkeepercontactnumber).value = '';
			ReactDOM.findDOMNode(this.refs.shopkeeperusername).value = '';
			ReactDOM.findDOMNode(this.refs.shopkeeperpassword).value = '';

			}
		})


  }

	render() {


		return (
			<div className="box box-success">
		        <div className="box-header with-border">
		          <h3 className="box-title">Insert Shopkeeper</h3>

		        </div>
	<form className="new-task" onSubmit={this.handleSubmit.bind(this)} >
		        <div className="box-body">

							<div class="row">
            <div class="col-md-6">
							<div className="form-group">
									 <label>Shopkeeper Name</label>
									 <input type="text" ref="shopkeepername" className="form-control" placeholder="Enter Shopkeeper Name"/>
								 </div>

						<div className="form-group">
								 <label>Shopkeeper Address</label>
								 <input type="text" ref="shopkeeperaddress" className="form-control" placeholder="Enter Shopkeeper Address"/>
							 </div>
							 <div className="form-group">
										<label>Shopkeeper Contact #</label>
										<input type="text" ref="shopkeepercontactnumber" className="form-control" placeholder="Enter Shopkeeper Contact #"/>
									</div>
							 	</div>

						<div class="col-md-6">
							<div className="form-group">
									 <label>Shopkeeper Username</label>
									 <input type="text" ref="shopkeeperusername" className="form-control" placeholder="Enter Shopkeeper Username"/>
								 </div>
								 <div className="form-group">
											<label>Shopkeeper Password</label>
											<input type="text" ref="shopkeeperpassword" className="form-control" placeholder="Enter Shopkeeper Password"/>
										</div>
						</div>
						</div>
					</div>



								<div class="box-footer">

		                 <button type="submit" class="btn btn-success pull-right">Create</button>
		               </div>
									 </form>
		      </div>

		)
	}

}
