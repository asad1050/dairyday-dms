import React, {Component} from 'react'
import ReactDOM from 'react-dom'

import TrackerReact from 'meteor/ultimatejs:tracker-react';


export default class CartList extends TrackerReact(React.Component) {

	constructor(props){
		super(props)
		this.state = {}
	}

	completetransaction(event){
		event.preventDefault();

		const cart = Session.get('cart');
		const carttotal = Session.get('carttotal');
		const cartprofit = Session.get('cartprofit');
		var date = new Date();
		date = date.toISOString();
		Meteor.call('insertTransaction',cart,carttotal,cartprofit, date,(err,res)=>{
			if (err){
				swal("Error", err.message, "error");
			}else{
				Session.setPersistent('cart',[]);
				Session.setPersistent('carttotal',0);
				Session.setPersistent('cartprofit',0);
			swal("Success", "Transaction completed successfully.", "success");
			// Clear form

			ReactDOM.findDOMNode(this.refs.upditemsold).value = '';

			}
		})
	}
	canceltransaction(event){
		event.preventDefault();
		Session.setPersistent('cart',[]);
		Session.setPersistent('carttotal',0);
		Session.setPersistent('cartprofit',0);
		swal("Success", "Transaction canceled successfully.", "success");
	}

	render() {

		var carts = Session.get('cart');
		var carttotal = Session.get('carttotal');
		return (
			<div className="box box-success">
	            <div className="box-header">
	              <h3 className="box-title">Cart</h3>

	            </div>

	            <div className="box-body table-responsive no-padding">
	              <table className="table table-hover">
	                { carts && <tbody>
										<tr>

	                  <th>Name</th>
	                  <th>Rate</th>
										<th>Quantity</th>
										<th>Price</th>
	                </tr>

									{carts.map(function(cart, index){
									return ( <tr key={ index }>
 	                  <td>{cart.itemname}</td>
 	                  <td>{cart.itemrate}</td>
										<td>{cart.quantity}</td>
										<td>{cart.itemprice}</td>

 	                </tr>);
									})}

									<tr>
										<td>Total Price: </td>
									  <td> </td>
									  <td> </td>
									  <td>{carttotal} </td>
								</tr>


	              </tbody>}
							</table>
	            </div>
							<div className="box-footer" >

								<form onSubmit={this.completetransaction.bind(this)} >

								<button type="submit" className="btn btn-success pull-right">Complete Transaction</button>
						</form>

						<form onSubmit={this.canceltransaction.bind(this)} >

						<button type="submit" className="btn btn-danger pull-left">Cancel Transaction</button>
				</form>
							</div>

	          </div>

		)
	}

}
