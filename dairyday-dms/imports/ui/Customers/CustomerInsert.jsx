import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import swal from 'sweetalert'


export default class CustomerInsert extends Component {
	constructor(props){
		super(props)
	}

	handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
    const customername = ReactDOM.findDOMNode(this.refs.customername).value.trim();
		const customeraddress = ReactDOM.findDOMNode(this.refs.customeraddress).value.trim();
		const customercontactnumber = ReactDOM.findDOMNode(this.refs.customercontactnumber).value.trim();
		const customerusername = ReactDOM.findDOMNode(this.refs.customerusername).value.trim();
		const customerpassword = ReactDOM.findDOMNode(this.refs.customerpassword).value.trim();

		Meteor.call('createCustomer',customername,customeraddress, customercontactnumber, customerusername, customerpassword,(err,res)=>{
			if (err){
				swal("Error", err.message, "error");
			}else{
			swal("Success", "Customer successfully created.", "success");
			// Clear form
	    ReactDOM.findDOMNode(this.refs.customername).value = '';
			ReactDOM.findDOMNode(this.refs.customeraddress).value = '';
			ReactDOM.findDOMNode(this.refs.customercontactnumber).value = '';
			ReactDOM.findDOMNode(this.refs.customerusername).value = '';
			ReactDOM.findDOMNode(this.refs.customerpassword).value = '';

			}
		})


  }

	render() {


		return (
			<div className="box box-success">
		        <div className="box-header with-border">
		          <h3 className="box-title">Insert Customer</h3>

		        </div>
	<form className="new-task" onSubmit={this.handleSubmit.bind(this)} >
		        <div className="box-body">

							<div class="row">
            <div class="col-md-6">
							<div className="form-group">
									 <label>Customer Name</label>
									 <input type="text" ref="customername" className="form-control" placeholder="Enter Customer Name"/>
								 </div>

						<div className="form-group">
								 <label>Customer Address</label>
								 <input type="text" ref="customeraddress" className="form-control" placeholder="Enter Customer Address"/>
							 </div>
							 <div className="form-group">
										<label>Customer Contact #</label>
										<input type="text" ref="customercontactnumber" className="form-control" placeholder="Enter Customer Contact #"/>
									</div>
							 	</div>

						<div class="col-md-6">
							<div className="form-group">
									 <label>Customer Username</label>
									 <input type="text" ref="customerusername" className="form-control" placeholder="Enter Customer Username"/>
								 </div>
								 <div className="form-group">
											<label>Customer Password</label>
											<input type="text" ref="customerpassword" className="form-control" placeholder="Enter Customer Password"/>
										</div>
						</div>
						</div>
					</div>



								<div class="box-footer">

		                 <button type="submit" class="btn btn-success pull-right">Create</button>
		               </div>
									 </form>
		      </div>

		)
	}

}
