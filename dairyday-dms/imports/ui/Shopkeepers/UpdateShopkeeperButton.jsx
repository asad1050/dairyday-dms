import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import {Shopkeepers} from '../../api/collections'

export default class UpdateShopkeeperButton extends Component {

	constructor(props){
		super(props)
		this.state = {
          subscription: {
            shopkeepers: Meteor.subscribe('shopkeepers')
          }
        }
	}

	componentWillUnmount() {
		this.state.subscription.shopkeepers.stop();
	}

	handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
    const id = ReactDOM.findDOMNode(this.refs.id).value.trim();
		var shopkeeper = Shopkeepers.findOne({'_id':id});
		document.getElementById("updshopkeepername").value = shopkeeper.name;
		document.getElementById("updshopkeeperaddress").value = shopkeeper.address;
		document.getElementById("updshopkeepercontactnumber").value = shopkeeper.contactnumber;
		document.getElementById("updshopkeeperid").value = id;

  }

	render() {

		return (
			<div>
				<form  onSubmit={this.handleSubmit.bind(this)} >
					<input type="hidden" value={this.props.id} ref="id" />
					<button type="submit"  class="btn btn-default btn-block" data-toggle="modal" data-target="#modal-default">
												<i class="fa fa-edit"></i>
											 </button>
				</form>
			</div>

		)
	}

}
