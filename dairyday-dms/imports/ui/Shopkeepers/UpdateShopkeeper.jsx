import React, {Component} from 'react'
import ReactDOM from 'react-dom'


export default class UpdateShopkeeper extends Component {

	constructor(props){
		super(props)

	}

	handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
		const shopkeepername = ReactDOM.findDOMNode(this.refs.updshopkeepername).value.trim();
		const shopkeeperaddress = ReactDOM.findDOMNode(this.refs.updshopkeeperaddress).value.trim();
		const shopkeepercontactnumber = ReactDOM.findDOMNode(this.refs.updshopkeepercontactnumber).value.trim();
		const shopkeeperid = ReactDOM.findDOMNode(this.refs.updshopkeeperid).value.trim();
		const shopkeeperpassword = ReactDOM.findDOMNode(this.refs.updshopkeeperpassword).value.trim();

		Meteor.call('updateShopkeeper',shopkeeperid, shopkeepername,shopkeeperaddress, shopkeepercontactnumber, shopkeeperpassword,(err,res)=>{
			if (err){
				swal("Error", err.message, "error");
			}else{
			swal("Success", "Shopkeeper successfully Updated.", "success");
			document.getElementById("updmodalclose").click();
			ReactDOM.findDOMNode(this.refs.updshopkeepername).value = '';
			ReactDOM.findDOMNode(this.refs.updshopkeeperaddress).value = '';
			ReactDOM.findDOMNode(this.refs.updshopkeepercontactnumber).value = '';
			ReactDOM.findDOMNode(this.refs.updshopkeeperid).value = '';
			ReactDOM.findDOMNode(this.refs.updshopkeeperpassword).value = '';
			}
		})

  }

	render() {

		return (
			<div>
				<div class="modal fade" id="modal-default">
				 <div class="modal-dialog">
					 <div class="modal-content">
						 <div class="modal-header">
							 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
								 <span aria-hidden="true">&times;</span></button>
							 <h4 class="modal-title">Update Shopkeeper</h4>
						 </div>
						 	<form  onSubmit={this.handleSubmit.bind(this)} >
						 <div class="modal-body">
							 <div className="form-group">
 									 <label>Shopkeeper Name</label>
 									 <input type="text" id="updshopkeepername" ref="updshopkeepername" className="form-control" placeholder="Enter Shopkeeper Name"/>
 								 </div>
								 <input type="hidden" id="updshopkeeperid" ref="updshopkeeperid" className="form-control" placeholder="Enter shopkeeper Name"/>

 						<div className="form-group">
 								 <label>Shopkeeper Address</label>
 								 <input type="text" id="updshopkeeperaddress" ref="updshopkeeperaddress" className="form-control" placeholder="Enter Shopkeeper Address"/>
 							 </div>
 							 <div className="form-group">
 										<label>Shopkeeper Contact #</label>
 										<input type="text" id="updshopkeepercontactnumber" ref="updshopkeepercontactnumber" className="form-control" placeholder="Enter Shopkeeper Contact #"/>
 									</div>

										 <div className="form-group">
													<label>Shopkeeper Password</label>
													<input type="text" id="updshopkeeperpassword" ref="updshopkeeperpassword" className="form-control" placeholder="Enter Password if you want to change"/>
												</div>
						 </div>
						 <div class="modal-footer">
							 <button type="button" id="updmodalclose" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
							 <button type="submit" class="btn btn-primary" >Save changes</button>
						 </div>
					 </form>
					 </div>

				 </div>

			 </div>

			</div>

		)
	}

}
