import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import {Items} from '../../api/collections'

import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class ItemQuantity extends Component {

	constructor(props){
		super(props)
		this.state = {
          subscription: {
            items: Meteor.subscribe('items')
          }
        }
	}

	componentWillUnmount() {
		this.state.subscription.items.stop();
	}

	handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
		const itemname = ReactDOM.findDOMNode(this.refs.upditemname).value.trim();
		const itemprice = ReactDOM.findDOMNode(this.refs.upditemprice).value.trim();
		const itemsold = ReactDOM.findDOMNode(this.refs.upditemsold).value.trim();
		const itemid = ReactDOM.findDOMNode(this.refs.upditemid).value.trim();
		var itemcost = Items.findOne({'_id':itemid}).cost;
		var quantity = (parseFloat(itemsold) / parseFloat(itemprice));
		quantity = parseFloat(quantity.toFixed(3));
		if (Session.get('cartprofit')){}else{
			Session.setPersistent('cartprofit',0);
		}
		if (Session.get('carttotal')){}else{
			Session.setPersistent('carttotal',0);
		}
		if (Session.get('cart')){
			var cart = Session.get('cart');
			var carttotal = Session.get('carttotal');
			var profit = Session.get('cartprofit');
			cart.push({ 'itemid':itemid,'itemname':itemname, 'itemrate':itemprice,'quantity':quantity, 'itemprice':itemsold, 'itemcost':itemcost });
			carttotal = parseFloat(carttotal) + parseFloat(itemsold);
			const rprofit = ((parseFloat(itemprice)- parseFloat(itemcost)) * parseFloat(quantity));
			profit = parseFloat(profit) + parseFloat(rprofit);
			profit = profit.toFixed(3);
			Session.setPersistent('carttotal',carttotal);
			Session.setPersistent('cartprofit',profit);
			Session.setPersistent('cart',cart);
			document.getElementById("updmodalclose").click();
			ReactDOM.findDOMNode(this.refs.upditemsold).value = "";
		}else{
			Session.setPersistent('cart',[]);
		}

  }

	render() {

		return (
			<div>
				<div class="modal fade" id="modal-default">
				 <div class="modal-dialog">
					 <div class="modal-content">
						 <div class="modal-header">
							 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
								 <span aria-hidden="true">&times;</span></button>
							 <h4 class="modal-title">Update Item</h4>
						 </div>
						 	<form  onSubmit={this.handleSubmit.bind(this)} >
						 <div class="modal-body">
							 <div className="form-group">
 									 <label>Item Name</label>
 									 <input type="text" id="upditemname" ref="upditemname" className="form-control" placeholder="Enter Item Name" disabled />
 								 </div>
								 <input type="hidden" id="upditemid" ref="upditemid" className="form-control" placeholder="Enter Item Name"/>

 						<div className="form-group">
 								 <label>Item Price</label>
 								 <input type="text" id="upditemprice" ref="upditemprice" className="form-control" placeholder="Enter Item Price" disabled />
 							 </div>
							 <div className="form-group">
											<label>Item Sold Price</label>
											<input type="text" id="upditemsold" ref="upditemsold" className="form-control" placeholder="Enter Item sale price"/>
										</div>

						 </div>
						 <div class="modal-footer">
							 <button type="button" id="updmodalclose" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
							 <button type="submit" class="btn btn-primary" >Add to Cart</button>
						 </div>
					 </form>
					 </div>

				 </div>

			 </div>

			</div>

		)
	}

}
