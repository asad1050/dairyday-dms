import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import swal from 'sweetalert'
import {Items,Customers,DeliveryBoys} from '../../api/collections'
import TrackerReact from 'meteor/ultimatejs:tracker-react';


export default class DeliveryInsert extends TrackerReact(React.Component) {
	constructor(props){
		super(props)
		this.state = {
          subscription: {
            items: Meteor.subscribe('items'),
						customers: Meteor.subscribe('customers'),
						deliveryboys: Meteor.subscribe('deliveryboys')
          }
        }
	}

	componentWillUnmount() {
		this.state.subscription.items.stop();
		this.state.subscription.customers.stop();
		this.state.subscription.deliveryboys.stop();
	}

	items(){
		return Items.find().fetch();
	}
	customers(){
		return Customers.find().fetch();
	}
	deliveryboys(){
		return DeliveryBoys.find().fetch();
	}

	handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
    const itemid = ReactDOM.findDOMNode(this.refs.itemid).value.trim();
		const customerid = ReactDOM.findDOMNode(this.refs.customerid).value.trim();
		const quantity = ReactDOM.findDOMNode(this.refs.quantity).value.trim();
		var date = new Date();
		date = date.toISOString();
		var deliveryboyid = DeliveryBoys.findOne({ 'userid':Meteor.userId()})._id;
		Meteor.call('insertDelivery',itemid,customerid, deliveryboyid,quantity,date,(err,res)=>{
			if (err){
				swal("Error", err.message, "error");
			}else{
			swal("Success", "Delivered successfully.", "success");
			// Clear form

			ReactDOM.findDOMNode(this.refs.quantity).value = '';

			}
		})


  }

	render() {
		let items = this.items();
		let customers = this.customers();
		let deliveryboys = this.deliveryboys();
		return (
			<div className="box box-success">
		        <div className="box-header with-border">
		          <h3 className="box-title">Insert Quantity</h3>

		        </div>
	<form className="new-task" onSubmit={this.handleSubmit.bind(this)} >
		        <div className="box-body">

							<div class="row">
  <div class="col-md-12">

		<div class="form-group">
				<label>Select Item</label>
				<select ref="itemid" class="form-control">
					{items.map(function(item, index){
					return (<option key={ index } value={item._id} >{item.name}</option>);
					})}

				</select>
			</div>

			<div class="form-group">
					<label>Select Customer</label>
					<select ref="customerid" class="form-control">
						{customers.map(function(customer, index){
						return (<option key={ index } value={customer._id} >{customer.name} - {customer.contactnumber}</option>);
						})}

					</select>
				</div>

						<div className="form-group">
								 <label>Quantity</label>
								 <input type="text" ref="quantity" className="form-control" placeholder="Enter Quantity"/>
							 </div>
</div>
						</div>
					</div>



								<div class="box-footer">

		                 <button type="submit" class="btn btn-success pull-right">Insert</button>
		               </div>
									 </form>
		      </div>

		)
	}

}
