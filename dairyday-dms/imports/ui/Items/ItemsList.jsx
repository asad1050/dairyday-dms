import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import {Items} from '../../api/collections'
import DeleteItem from './DeleteItem'
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import UpdateItemButton from './UpdateItemButton'


export default class ItemsList extends TrackerReact(React.Component) {

	constructor(props){
		super(props)
		this.state = {
          subscription: {
            items: Meteor.subscribe('items')
          }
        }
	}

	componentWillUnmount() {
		this.state.subscription.items.stop();
	}

	items(){
		return Items.find().fetch();
	}

	handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
    const id = ReactDOM.findDOMNode(this.refs.id).value.trim();

    console.log(id);

  }

	render() {
		let items = this.items();


		return (

			<div className="box box-info">
	            <div className="box-header">
	              <h3 className="box-title">Items</h3>

	            </div>

	            <div className="box-body table-responsive no-padding">
	              <table className="table table-hover">
	                <tbody>
										<tr>

	                  <th>Name</th>
	                  <th>Price</th>
										<th>Cost</th>
										<th>Update</th>
										<th>Delete</th>
	                </tr>
									{items.map(function(item, index){
									return ( <tr key={ index }>
 	                  <td>{item.name}</td>
 	                  <td>{item.price}</td>
										<td>{item.cost}</td>
										<td>
											<UpdateItemButton id={item._id} />
										</td>
 	                  <td>
											<DeleteItem id={item._id} />
										</td>
 	                </tr>);
									})}


	              </tbody></table>
	            </div>

	          </div>

		)
	}

}
