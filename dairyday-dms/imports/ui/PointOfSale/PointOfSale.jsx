import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import ItemsList from './ItemsList'
import ItemQuantity from './ItemQuantity'
import CartList from './CartList'


export default class PointOfSale extends Component {
	constructor(props){
		super(props)
	}


	render() {


		return (
		<div>
		<ItemsList/>
		<ItemQuantity/>
		<CartList/>
		</div>

		)
	}

}
