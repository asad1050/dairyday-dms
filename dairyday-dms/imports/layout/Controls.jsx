import React, {Component} from 'react'
import Dashboard from '../ui/Dashboard/Dashboard'
import Items from '../ui/Items/Items'
import Customers from '../ui/Customers/Customers'
import ChangePassword from '../ui/ChangePassword/ChangePassword'
import Expenses from '../ui/Expenses/Expenses'
import Shopkeepers from '../ui/Shopkeepers/Shopkeepers'
import DeliveryBoys from '../ui/DeliveryBoys/DeliveryBoys'
import StockIn from '../ui/StockIn/StockIn'
import PointOfSale from '../ui/PointOfSale/PointOfSale'
import DeliveryBoyStockIn from '../ui/DeliveryBoyStockIn/StockIn'
import Delivery from '../ui/Delivery/Delivery'
import BalanceDeposit from '../ui/BalanceDeposit/BalanceDeposit'
import Billing from '../ui/Reports/Billing'
import CustomersBalanceReport from '../ui/Reports/CustomersBalanceReport'
import AccountsReport from '../ui/Reports/AccountsReport'
import AdminPaymentReport from '../ui/Reports/AdminPaymentReport'
import CustomerPaymentReport from '../ui/Reports/CustomerPaymentReport'
import ProfitLossReport from '../ui/Reports/ProfitLossReport'
import ReactDOM from 'react-dom'
import {Users} from '../api/collections'
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class Controls extends TrackerReact(React.Component) {
	constructor(props){
		super(props)
		this.state = {
          subscription: {
            users: Meteor.subscribe('user')
          }
        }

	}
	componentWillUnmount() {
		this.state.subscription.users.stop();
	}

	users(){
		return Users.find().fetch();
	}



	handleSignout(event) {
    event.preventDefault();

		Meteor.logout((err,res)=>{
			if (err){
				console.log(err.message);
				swal("Error", err.message, "error");
			}else{

				FlowRouter.go('/login')
			}
		});
  }

	navDashboard(event){
		FlowRouter.go('/');
		document.body.classList.remove('sidebar-open');
		var elements = document.getElementsByTagName("li");
for (var ii=0; ii < elements.length; ii++) {

    elements[ii].classList.remove("active");

}
		console.log(ReactDOM.findDOMNode(this.refs.navDashboard).parentNode.classList.add('active'));
	}
	navItems(event){
		FlowRouter.go('/items');
		document.body.classList.remove('sidebar-open');
		var elements = document.getElementsByTagName("li");
for (var ii=0; ii < elements.length; ii++) {

    elements[ii].classList.remove("active");

}
		console.log(ReactDOM.findDOMNode(this.refs.navItems).parentNode.classList.add('active'));
	}
	navCustomers(event){
		FlowRouter.go('/customers');
		document.body.classList.remove('sidebar-open');
		var elements = document.getElementsByTagName("li");
for (var ii=0; ii < elements.length; ii++) {

    elements[ii].classList.remove("active");

}
		console.log(ReactDOM.findDOMNode(this.refs.navCustomers).parentNode.classList.add('active'));
	}
	navDeliveryBoys(event){
		FlowRouter.go('/deliveryboys');
		document.body.classList.remove('sidebar-open');
		var elements = document.getElementsByTagName("li");
for (var ii=0; ii < elements.length; ii++) {

    elements[ii].classList.remove("active");

}
		console.log(ReactDOM.findDOMNode(this.refs.navDeliveryBoys).parentNode.classList.add('active'));
	}

	navStockIn(event){
		FlowRouter.go('/stockin');
		document.body.classList.remove('sidebar-open');
		var elements = document.getElementsByTagName("li");
for (var ii=0; ii < elements.length; ii++) {

    elements[ii].classList.remove("active");

}
		console.log(ReactDOM.findDOMNode(this.refs.navStockIn).parentNode.classList.add('active'));
	}

	navCustomersBalanceReport(event){
		FlowRouter.go('/customersbalancereport');
		document.body.classList.remove('sidebar-open');
		var elements = document.getElementsByTagName("li");
for (var ii=0; ii < elements.length; ii++) {

    elements[ii].classList.remove("active");

}
		console.log(ReactDOM.findDOMNode(this.refs.navCustomersBalanceReport).parentNode.classList.add('active'));
	}

	navAccountsReport(event){
		FlowRouter.go('/accountsreport');
		document.body.classList.remove('sidebar-open');
		var elements = document.getElementsByTagName("li");
for (var ii=0; ii < elements.length; ii++) {

    elements[ii].classList.remove("active");

}
		console.log(ReactDOM.findDOMNode(this.refs.navAccountsReport).parentNode.classList.add('active'));
	}




	navBilling(event){
		FlowRouter.go('/billing');
		document.body.classList.remove('sidebar-open');
		var elements = document.getElementsByTagName("li");
for (var ii=0; ii < elements.length; ii++) {

    elements[ii].classList.remove("active");

}
		console.log(ReactDOM.findDOMNode(this.refs.navBilling).parentNode.classList.add('active'));
	}

	navDelivery(event){
		FlowRouter.go('/delivery');
		document.body.classList.remove('sidebar-open');
		var elements = document.getElementsByTagName("li");
for (var ii=0; ii < elements.length; ii++) {

    elements[ii].classList.remove("active");

}
		console.log(ReactDOM.findDOMNode(this.refs.navDelivery).parentNode.classList.add('active'));
	}

	navBalanceDeposit(event){
		FlowRouter.go('/balancedeposit');
		document.body.classList.remove('sidebar-open');
		var elements = document.getElementsByTagName("li");
for (var ii=0; ii < elements.length; ii++) {

    elements[ii].classList.remove("active");

}
		console.log(ReactDOM.findDOMNode(this.refs.navBalanceDeposit).parentNode.classList.add('active'));
	}

	navShopkeepers(event){
		FlowRouter.go('/shopkeepers');
		document.body.classList.remove('sidebar-open');
		var elements = document.getElementsByTagName("li");
for (var ii=0; ii < elements.length; ii++) {

    elements[ii].classList.remove("active");

}
		console.log(ReactDOM.findDOMNode(this.refs.navShopkeepers).parentNode.classList.add('active'));
	}

	navExpenses(event){
		FlowRouter.go('/expenses');
		document.body.classList.remove('sidebar-open');
		var elements = document.getElementsByTagName("li");
for (var ii=0; ii < elements.length; ii++) {

    elements[ii].classList.remove("active");

}
		console.log(ReactDOM.findDOMNode(this.refs.navExpenses).parentNode.classList.add('active'));
	}

	navDeliveryBoyStockIn(event){
		FlowRouter.go('/deliveryboystock');
		document.body.classList.remove('sidebar-open');
		var elements = document.getElementsByTagName("li");
for (var ii=0; ii < elements.length; ii++) {

    elements[ii].classList.remove("active");

}
		console.log(ReactDOM.findDOMNode(this.refs.navDeliveryBoyStockIn).parentNode.classList.add('active'));
	}

	navChangePassword(event){
		FlowRouter.go('/changepassword');
		document.body.classList.remove('sidebar-open');
		var elements = document.getElementsByTagName("li");
for (var ii=0; ii < elements.length; ii++) {

    elements[ii].classList.remove("active");

}
		console.log(ReactDOM.findDOMNode(this.refs.navChangePassword).parentNode.classList.add('active'));
	}

	navPointOfSale(event){
		FlowRouter.go('/pointofsale');
		document.body.classList.remove('sidebar-open');
		var elements = document.getElementsByTagName("li");
for (var ii=0; ii < elements.length; ii++) {

    elements[ii].classList.remove("active");

}
		console.log(ReactDOM.findDOMNode(this.refs.navPointOfSale).parentNode.classList.add('active'));
	}

	navProfitLossReport(event){
		FlowRouter.go('/profitlossreport');
		document.body.classList.remove('sidebar-open');
		var elements = document.getElementsByTagName("li");
for (var ii=0; ii < elements.length; ii++) {

    elements[ii].classList.remove("active");

}
		console.log(ReactDOM.findDOMNode(this.refs.navProfitLossReport).parentNode.classList.add('active'));
	}

	navAdminPaymentReport(event){
		FlowRouter.go('/adminpaymentreport');
		document.body.classList.remove('sidebar-open');
		var elements = document.getElementsByTagName("li");
for (var ii=0; ii < elements.length; ii++) {

    elements[ii].classList.remove("active");

}
		console.log(ReactDOM.findDOMNode(this.refs.navAdminPaymentReport).parentNode.classList.add('active'));
	}

	navCustomerPaymentReport(event){
		FlowRouter.go('/customerpaymentreport');
		document.body.classList.remove('sidebar-open');
		var elements = document.getElementsByTagName("li");
for (var ii=0; ii < elements.length; ii++) {

    elements[ii].classList.remove("active");

}
		console.log(ReactDOM.findDOMNode(this.refs.navCustomerPaymentReport).parentNode.classList.add('active'));
	}

	componentDidMount(){



		document.body.classList.add('hold-transition');
		document.body.classList.add('skin-blue');
		document.body.classList.add('sidebar-mini');
		document.body.classList.remove('login-page');

		if (this.props.comp == "App"){
			this.state.comp = "App";
		}
	}

	render() {
		let type = "blank";
		let name = "blank";
		let myname = this.props.comp;
		if (Meteor.user()){
			if (Meteor.user().type){
		type = Meteor.user().type;
		name = Meteor.user().username;
	}
	}

		return(
			<div className="wrapper">

  <header className="main-header">

    <a href="" className="logo">

      <span className="logo-mini"><b>D</b>D</span>

      <span className="logo-lg"><b>Dairy</b>Day</span>
    </a>

    <nav className="navbar navbar-static-top">

      <a href="#" className="sidebar-toggle" data-toggle="push-menu" role="button">
        <span className="sr-only">Toggle navigation</span>
        <span className="icon-bar"></span>
        <span className="icon-bar"></span>
        <span className="icon-bar"></span>
      </a>

      <div className="navbar-custom-menu">
        <ul className="nav navbar-nav">

          <li className="dropdown messages-menu">


          </li>

          <li className="dropdown notifications-menu">

            <ul className="dropdown-menu">

            </ul>
          </li>

          <li className="dropdown tasks-menu">

            <ul className="dropdown-menu">

              <li>

                <ul className="menu">
                  <li>

                  </li>

                </ul>
              </li>
              <li className="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>

          <li className="dropdown user user-menu">
            <a href="#" className="dropdown-toggle" data-toggle="dropdown">
              <img src="dist/img/avatar5.png" className="user-image" alt="User Image"/>
              <span className="hidden-xs">{name}</span>
            </a>
            <ul className="dropdown-menu">

              <li className="user-header">
                <img src="dist/img/avatar5.png" className="img-circle" alt="User Image"/>

                <p>
                  {name}
                  <small>{type}</small>
                </p>
              </li>


              <li className="user-footer">

                <div className="pull-right">
									  <form onSubmit={this.handleSignout.bind(this)} >

									  <button type="submit" className="btn btn-default btn-flat">Sign out</button>
								</form>
                </div>
              </li>
            </ul>
          </li>

        </ul>
      </div>
    </nav>
  </header>




  <aside className="main-sidebar">

    <section className="sidebar">

      <div className="user-panel">
        <div className="pull-left image">
          <img src="dist/img/avatar5.png" className="img-circle" alt="User Image"></img>
        </div>
        <div className="pull-left info">
          <p>{name}</p>
					{type}
        </div>
      </div>




				{(() => {
	        switch (type) {
	          case "admin":   return (
							<ul className="sidebar-menu" data-widget="tree">
				        <li  className="header">MAIN NAVIGATION</li>
							<li className="active" >
								<a href="" ref="navDashboard" onClick={this.navDashboard.bind(this)}>
									<i className="fa fa-dashboard"></i> <span>Dashboard</span>

								</a>
							</li>
							<li>
								<a href="" ref="navChangePassword" onClick={this.navChangePassword.bind(this)}>
									<i className="fa fa-key"></i> <span>Change Password</span>

								</a>
							</li>
							<li>
								<a href="" ref="navStockIn" onClick={this.navStockIn.bind(this)}>
									<i className="fa fa-cube"></i> <span>StockIn (Shop)</span>

								</a>
							</li>
							<li>
								<a href="" ref="navDeliveryBoyStockIn" onClick={this.navDeliveryBoyStockIn.bind(this)}>
									<i className="fa  fa-cubes"></i> <span>StockIn (DeliveryBoy)</span>

								</a>
							</li>
							<li>
								<a href="" ref="navExpenses" onClick={this.navExpenses.bind(this)}>
									<i className="fa fa-money"></i> <span>Expenses</span>

								</a>
							</li>
							<li>
								<a href="" ref="navItems" onClick={this.navItems.bind(this)}>
									<i className="fa fa-cubes"></i> <span>Items</span>

								</a>
							</li>
							<li>
								<a href="" ref="navCustomers" onClick={this.navCustomers.bind(this)}>
									<i className="fa fa-users"></i> <span>Customers</span>

								</a>
							</li>
							<li>
								<a href="" ref="navShopkeepers" onClick={this.navShopkeepers.bind(this)}>
									<i className="fa fa-user-plus"></i> <span>Shopkeepers</span>

								</a>
							</li>
							<li>
								<a href="" ref="navDeliveryBoys" onClick={this.navDeliveryBoys.bind(this)}>
									<i className="fa fa-truck"></i> <span>Delivery Boys</span>

								</a>
							</li>
							<li>
								<a href="" ref="navCustomersBalanceReport" onClick={this.navCustomersBalanceReport.bind(this)}>
									<i className="fa fa-table"></i> <span>Customer's Balance Report</span>

								</a>
							</li>
							<li>
								<a href="" ref="navAccountsReport" onClick={this.navAccountsReport.bind(this)}>
									<i className="fa fa-table"></i> <span>Accounts Report</span>

								</a>
							</li>
							<li>
								<a href="" ref="navProfitLossReport" onClick={this.navProfitLossReport.bind(this)}>
									<i className="fa fa-table"></i> <span>ProfitLoss Report</span>

								</a>
							</li>

							<li>
								<a href="" ref="navAdminPaymentReport" onClick={this.navAdminPaymentReport.bind(this)}>
									<i className="fa fa-table"></i> <span>Payment Report</span>

								</a>
							</li>


							  </ul>
						);
	          case "customer": return (
							<ul  className="sidebar-menu" data-widget="tree">
				        <li className="header">MAIN NAVIGATION</li>
							<li className="active" >
								<a href="" ref="navDashboard" onClick={this.navDashboard.bind(this)}>
									<i className="fa fa-dashboard"></i> <span>Dashboard</span>

								</a>
							</li>

							<li>
								<a href="" ref="navChangePassword" onClick={this.navChangePassword.bind(this)}>
									<i className="fa fa-key"></i> <span>Change Password</span>

								</a>
							</li>

							<li>
								<a href="" ref="navBilling" onClick={this.navBilling.bind(this)}>
									<i className="fa fa-list-alt"></i> <span>Billing</span>

								</a>
							</li>

							<li>
								<a href="" ref="navCustomerPaymentReport" onClick={this.navCustomerPaymentReport.bind(this)}>
									<i className="fa fa-table"></i> <span>Payment Report</span>

								</a>
							</li>
  </ul>
						);
	          case "deliveryboy":  return (
							<ul className="sidebar-menu" data-widget="tree">
				        <li className="header">MAIN NAVIGATION</li>
							<li className="active" >
			          <a href="" ref="navDashboard" onClick={this.navDashboard.bind(this)}>
			            <i className="fa fa-dashboard"></i> <span>Dashboard</span>

			          </a>
			        </li>
							<li>
								<a href="" ref="navChangePassword" onClick={this.navChangePassword.bind(this)}>
									<i className="fa fa-key"></i> <span>Change Password</span>

								</a>
							</li>
							<li>
								<a href="" ref="navDelivery" onClick={this.navDelivery.bind(this)}>
									<i className="fa fa-credit-card"></i> <span>Delivery</span>

								</a>
							</li>
							<li>
								<a href="" ref="navBalanceDeposit" onClick={this.navBalanceDeposit.bind(this)}>
									<i className="fa fa-credit-card"></i> <span>Balance Deposit</span>

								</a>
							</li>
							  </ul>
						);
						case "shopkeeper":  return (
							<ul className="sidebar-menu" data-widget="tree">
				        <li className="header">MAIN NAVIGATION</li>
							<li className="active" >
			          <a href="" ref="navDashboard" onClick={this.navDashboard.bind(this)}>
			            <i className="fa fa-dashboard"></i> <span>Dashboard</span>

			          </a>
			        </li>
							<li>
								<a href="" ref="navChangePassword" onClick={this.navChangePassword.bind(this)}>
									<i className="fa fa-key"></i> <span>Change Password</span>

								</a>
							</li>
							<li>
								<a href="" ref="navStockIn" onClick={this.navStockIn.bind(this)}>
									<i className="fa fa-cube"></i> <span>StockIn (Shop)</span>

								</a>
							</li>
							<li>
								<a href="" ref="navDeliveryBoyStockIn" onClick={this.navDeliveryBoyStockIn.bind(this)}>
									<i className="fa  fa-cubes"></i> <span>StockIn (DeliveryBoy)</span>

								</a>
							</li>
							<li>
								<a href="" ref="navPointOfSale" onClick={this.navPointOfSale.bind(this)}>
									<i className="fa  fa-opencart"></i> <span>PointOfSale</span>

								</a>
							</li>

							  </ul>
						);
	          default:      return " Loading... ";
	        }
	      })()}


    </section>

  </aside>




  <div className="content-wrapper">

    <section className="content-header">
      <h1>
        {myname}
      </h1>

    </section>


    <section className="content">



			{(() => {
        switch (myname) {
          case "Dashboard":   return <Dashboard type={type} />;
          case "Items": return <Items/>;
          case "Customers":  return <Customers />;
					case "DeliveryBoys":  return <DeliveryBoys />;
					case "StockIn":  return <StockIn />;
					case "Delivery":  return <Delivery />;
					case "Billing":  return <Billing />;
					case "CustomersBalanceReport":  return <CustomersBalanceReport />;
					case "AccountsReport":  return <AccountsReport />;
					case "BalanceDeposit":  return <BalanceDeposit />;
					case "Shopkeepers":  return <Shopkeepers />;
					case "Expenses":  return <Expenses />;
					case "DeliveryBoyStockIn":  return <DeliveryBoyStockIn />;
					case "PointOfSale":  return <PointOfSale />;
					case "ProfitLossReport":  return <ProfitLossReport />;
					case "AdminPaymentReport":  return <AdminPaymentReport />;
					case "CustomerPaymentReport":  return <CustomerPaymentReport />;
					case "ChangePassword":  return <ChangePassword />;
          default:      return " ";
        }
      })()}


    </section>

  </div>


  <footer className="main-footer">
    <div className="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2018 DairyDay.</strong> | Powered by <a href="http://evarosoft.com" target="_blank" >Evarosoft</a>
  </footer>


  <div className="control-sidebar-bg"></div>
</div>
    );
	}

}
