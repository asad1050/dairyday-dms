import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import {Deliveries,Customers,Items,DeliveryBoys} from '../../api/collections'
import $ from 'jquery';
import moment from 'moment';
import 'bootstrap-datepicker';

import TrackerReact from 'meteor/ultimatejs:tracker-react';


export default class AccountsReport extends TrackerReact(React.Component) {

	constructor(props){
		super(props)
		this.state = {
          subscription: {
            deliveries: Meteor.subscribe('deliveries'),
						customers: Meteor.subscribe('customers'),
						items: Meteor.subscribe('items'),
						deliveryboys: Meteor.subscribe('deliveryboys')
          },
					deliveries:[],
					totalprice:0
        }
	}

	componentWillUnmount() {
		this.state.subscription.deliveries.stop();
		this.state.subscription.customers.stop();
		this.state.subscription.items.stop();
		this.state.subscription.deliveryboys.stop();
	}

	componentDidMount(){

		$('.datepicker').datepicker({
			format: 'mm-dd-yyyy',
        clearButton: true,
        weekStart: 1,
        time: false,
				autoclose: true
    });
	}

	customers(){
		return Customers.find().fetch();
	}

	deliveryboy(id){
		return DeliveryBoys.findOne({ '_id':id }).name;
	}
	customer(id){
		return Customers.findOne({ '_id':id }).name;
	}
	item(id){
		return Items.findOne({ '_id':id });
	}
	dateformat(date){
		return moment(date).format('MMMM Do YYYY');
	}

	handleSubmit(event) {
    event.preventDefault();
		const datefrom = ReactDOM.findDOMNode(this.refs.datefrom).value.trim();
		const dateto = ReactDOM.findDOMNode(this.refs.dateto).value.trim();
		var datef = new Date(datefrom);
		var datet = new Date(dateto);
		datef = datef.toISOString();
		datet = datet.toISOString();
		var deliveries = Deliveries.find({'date' : { $gte : datef, $lt: datet }}).fetch();
		console.log(deliveries);

		var ndeliveries = deliveries;

		var totalpr = 0;
		for (var i=0;i<deliveries.length;i++){
			var enitem = this.item(deliveries[i].itemid);
			var pricee = enitem.price * deliveries[i].quantity;
			ndeliveries[i].itemid = enitem.name;
			ndeliveries[i].date = this.dateformat(deliveries[i].date);
			ndeliveries[i].deliveryboyid = this.deliveryboy(deliveries[i].deliveryboyid);
			ndeliveries[i].customerid = this.customer(deliveries[i].customerid);
			ndeliveries[i].rate = enitem.price;
			ndeliveries[i].price = pricee;
			totalpr = totalpr + pricee;
		}

		this.setState({
			deliveries:ndeliveries,
			totalprice:totalpr
		 });

  }

	render() {
		let deliveries = this.state.deliveries;
		let customers = this.customers();
		const divStyle = {
		  paddingBottom: '50px'
		};
		return (
			<div>

				<div className="box box-info" >
								<div className="box-header">
									<h3 className="box-title">Information</h3>

								</div>

								<div className="box-body" style={divStyle}>
									<p>Select two dates to get you deliveries between two dates.<br></br> Thanks!</p>
								</div>




							</div>




			<div className="box box-info">
	            <div className="box-header">
	              <h3 className="box-title">Deliveries</h3>

	            </div>

							<div className="box-body" >
								<div class="row">
										<form className="new-task" onSubmit={this.handleSubmit.bind(this)} >
            <div class="col-md-6">
								<div className="form-group">

																<label>Date From:</label>

																<div className="input-group date">
																	<div className="input-group-addon">
																		<i className="fa fa-calendar"></i>
																	</div>
																	<input type="text" ref="datefrom" className="form-control pull-right datepicker" />
																</div>

															</div>


														</div>

														<div class="col-md-6">
																<div className="form-group">

																								<label>Date To:</label>

																								<div className="input-group date">
																									<div className="input-group-addon">
																										<i className="fa fa-calendar"></i>
																									</div>
																									<input type="text" ref="dateto" className="form-control pull-right datepicker" />
																								</div>

																							</div>


																						</div>
																							<div class="col-md-12">
<div className="form-group">
																						<button type="submit" class="btn btn-info pull-right">Retrive</button>
</div>
</div>
																					</form>
													</div>
							</div>

							<div className="box-body table-responsive no-padding">
							 <table className="table table-hover">
								 <tbody>
									 <tr>

									 <th>Item</th>
									 <th>DeliveryBoy</th>
									 <th>Customer</th>
									 <th>Date</th>
									 <th>Quantity</th>
									 <th>Rate</th>
									 <th>Price</th>
								 </tr>
								 {deliveries.map(function(delivery, index){
								 return ( <tr key={ index }>
										 <td>{delivery.itemid}</td>
									 <td>{delivery.deliveryboyid}</td>
									  <td>{delivery.customerid}</td>
										 <td>{delivery.date}</td>
											 <td>{delivery.quantity}</td>
											 <td>{delivery.rate}</td>
												<td>{delivery.price}</td>
									 </tr>);
								 })}
								 
									 <tr>
									<td>  <i className="fa fa-align-justify"></i>  </td>
								<td><i className="fa fa-align-justify"></i> </td>
									<td> <i className="fa fa-align-justify"></i> </td>
										<td> <i className="fa fa-align-justify"></i> </td>
										<td> <i className="fa fa-align-justify"></i> </td>
										<td> <i className="fa fa-align-justify"></i> </td>
										 <td><i className="fa fa-align-justify"></i> </td>
									 </tr>

									<tr>
								 <td>  Total Price:  </td>
							 <td></td>
								 <td> </td>
									 <td> </td>
									 <td> </td>
									 <td> </td>
										<td>{this.state.totalprice}</td>
									</tr>


							 </tbody></table>
						 </div>


	          </div>
						</div>
		)
	}

}
