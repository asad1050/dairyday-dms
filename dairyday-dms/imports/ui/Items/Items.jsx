import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import ItemInsert from './ItemInsert'
import ItemsList from './ItemsList'
import UpdateItem from './UpdateItem'

export default class Items extends Component {
	constructor(props){
		super(props)
	}


	render() {


		return (
		<div>
		<ItemInsert/>
		<ItemsList/>
		<UpdateItem/>
		</div>

		)
	}

}
