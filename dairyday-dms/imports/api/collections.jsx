import { Mongo } from 'meteor/mongo';

export const Users = Meteor.users;
export const StockIn = new Mongo.Collection('stockin');
export const Customers = new Mongo.Collection('customers');
export const Deliveries = new Mongo.Collection('deliveries');
export const DeliveryBoys = new Mongo.Collection('deliveryboys');
export const Items = new Mongo.Collection('items');
export const Payments = new Mongo.Collection('payments');
export const DeliveryBoyStock = new Mongo.Collection('deliveryboystock');
export const PointOfSales = new Mongo.Collection('pointofsales');
export const Expenses = new Mongo.Collection('expenses');
export const Shopkeepers = new Mongo.Collection('shopkeepers');
